package supervisor

import (
	"bufio"
	"errors"
	"os"
	"strings"

	"gitlab.com/keysaim/goinfra/log"
)

/*
;[program:theprogramname]
;command=/bin/cat            ; the program (relative uses PATH, can take args)
;priority=999                ; the relative start priority (default 999)
;autostart=true              ; start at supervisord start (default: true)
;autorestart=true            ; retstart at unexpected quit (default: true)
;startsecs=10                ; number of secs prog must stay running (def. 10)
;startretries=3              ; max # of serial start failures (default 3)
;exitcodes=0,2               ; 'expected' exit codes for process (default 0,2)
;stopsignal=QUIT             ; signal used to kill process (default TERM)
;stopwaitsecs=10             ; max num secs to wait before SIGKILL (default 10)
;user=chrism                 ; setuid to this UNIX account to run the program
;log_stdout=true             ; if true, log program stdout (default true)
;log_stderr=true             ; if true, log program stderr (def false)
;logfile=/var/log/cat.log    ; child log path, use NONE for none; default AUTO
;logfile_maxbytes=1MB        ; max # logfile bytes b4 rotation (default 50MB)
;logfile_backups=10          ; # of logfile backups (default 10)
*/
type Program struct {
	Name    string
	Command string
	LogFile string
	Others  map[string]string
}

type ConfigFile struct {
	dir string
}

func NewConfigFile(dir string) *ConfigFile {

	file, err := os.Open(dir)
	if err != nil {
		log.Fatal(err)
	}
	fi, err := file.Stat()
	if err != nil {
		log.Fatal(err)
	}
	if !fi.IsDir() {
		log.Fatalf("The config path: %s is not directory", dir)
	}
	if len(dir) == 0 {
		log.Fatal("Invalid config path: ", dir)
	}
	if dir[len(dir)-1] != "/"[0] {
		dir += "/"
	}
	return &ConfigFile{
		dir: dir,
	}
}

func (cf *ConfigFile) path(name string) string {

	return cf.dir + name + ".conf"
}

func (cf *ConfigFile) Add(p *Program) error {

	log.Debugf("Add program: %v", p)
	return cf.dump(cf.path(p.Name), p)
}

func (cf *ConfigFile) Adds(pl []*Program) {

	for _, p := range pl {
		cf.Add(p)
	}
}

func (cf *ConfigFile) Get(name string) (*Program, error) {

	return cf.load(cf.path(name))
}

func (cf *ConfigFile) Del(name string) {

	log.Debugf("Add program: %s", name)
	err := os.Remove(cf.path(name))
	if err != nil {
		log.Warn(err)
	}
}

func (cf *ConfigFile) load(path string) (*Program, error) {

	pl, err := cf.loads(path)
	if err != nil {
		return nil, err
	}
	if len(pl) == 0 {
		log.Error("No program in the file: ", path)
		return nil, errors.New("No program")
	}
	return pl[0], nil
}

func (cf *ConfigFile) loads(path string) ([]*Program, error) {

	file, err := os.Open(path)
	if err != nil {
		log.Error(err)
		return nil, err
	}

	defer file.Close()

	var pl []*Program
	var p *Program
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if line[0] == "#"[0] {
			continue
		}
		line = strings.Trim(line, " \t")
		if len(line) == 0 {
			continue
		}
		if line[0] == "["[0] {
			if p != nil {
				pl = append(pl, p)
				p = nil
			}
			segs := strings.Split(line, ":")
			if len(segs) < 2 || len(segs[1]) < 2 {
				log.Error("Invalid line: ", line)
				return nil, errors.New("Invalid line: " + line)
			}

			p = &Program{
				Name:   segs[1][:len(segs[1])-2],
				Others: make(map[string]string),
			}
		} else {
			segs := strings.Split(line, "=")
			if len(segs) < 2 {
				log.Error("Invalid line: ", line)
				return nil, errors.New("Invalid line: " + line)
			}
			fn := strings.Trim(segs[0], " \t")
			fv := strings.Trim(segs[1], " \t")
			switch fn {
			case "command":
				p.Command = fv
			case "logfile":
				p.LogFile = fv
			default:
				p.Others[fn] = fv
			}
		}
	}

	if p != nil {
		pl = append(pl, p)
	}
	return pl, nil
}

func (cf *ConfigFile) dump(path string, p *Program) error {

	return cf.dumps(path, []*Program{p})
}

func (cf *ConfigFile) dumps(path string, pl []*Program) error {

	file, err := os.Create(path)
	if err != nil {
		log.Error(err)
		return err
	}

	defer file.Close()

	for _, p := range pl {
		line := "[program:" + p.Name + "]\n"
		file.Write([]byte(line))
		line = "command=" + p.Command + "\n"
		file.Write([]byte(line))
		if len(p.LogFile) != 0 {
			line = "logfile=" + p.LogFile + "\n"
			file.Write([]byte(line))
		}
		for k, v := range p.Others {
			line = k + "=" + v + "\n"
			file.Write([]byte(line))
		}
	}
	return nil
}
