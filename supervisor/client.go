package supervisor

import (
	"bytes"
	"context"
	"fmt"
	"os/exec"

	"gitlab.com/keysaim/goinfra/log"
)

const (
	CT_DEL = iota
	CT_RESTART
	CT_START
	CT_STOP
	CT_RELOAD
)

type Command struct {
	Type int
	Name string
}

type Client struct {
	pc chan []*Program
	cc chan *Command
	cf *ConfigFile

	ctx    context.Context
	cancel context.CancelFunc
}

func NewClient(pctx context.Context, confDir string) *Client {

	c := &Client{
		pc: make(chan []*Program, 100),
		cc: make(chan *Command, 10),
		cf: NewConfigFile(confDir),
	}
	c.ctx, c.cancel = context.WithCancel(pctx)
	return c
}

func (c *Client) Init() {

	go func() {
		log.Info("The supervisor client running...")
		for {
			select {
			case pl := <-c.pc:
				c.adds(pl)
			case cmd := <-c.cc:
				c.onCommand(cmd)
			case <-c.ctx.Done():
				log.Infof("The supervisor client exit")
				return
			}
		}
	}()
}

func (c *Client) Close() {

	c.cancel()
}

func (c *Client) Add(p *Program) {

	log.Debugf("Add Program: %v", p)
	pl := []*Program{p}
	c.pc <- pl
}

func (c *Client) Adds(pl []*Program) {

	c.pc <- pl
}

func (c *Client) Del(name string) {

	log.Debugf("Delete Program: %s", name)
	cmd := &Command{CT_DEL, name}
	c.cc <- cmd
}

func (c *Client) Reload() {

	cmd := &Command{CT_RELOAD, ""}
	c.cc <- cmd
}

func (c *Client) reload() {

	log.Info("Reloading supervisor...")
	cmd := exec.Command("supervisorctl", "reload")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Error(err)
	}
	fmt.Printf("%q\n", out.String())
}

func (c *Client) adds(pl []*Program) {

	c.cf.Adds(pl)
}

func (c *Client) del(name string) {

	c.cf.Del(name)
}

func (c *Client) onCommand(cmd *Command) {

	log.Debugf("Handle supervisor client Command: %v", cmd)
	switch cmd.Type {
	case CT_DEL:
		c.del(cmd.Name)
	case CT_RELOAD:
		c.reload()
	default:
		log.Warnf("Unsupported command type: %d", cmd.Type)
	}
}
