package log

import (
	"bytes"
	"errors"
	"github.com/stretchr/testify/assert"
	_ "github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"testing"
)

type LogTestSuite struct {
	suite.Suite
}

func (ts *LogTestSuite) SetupSuite() {
	EnableLogRunTime(true)
	Info("SetupSuite")
}

func (ts *LogTestSuite) TearDownSuite() {
	Info("TeardownSuite")
}

func (ts *LogTestSuite) TestBasic() {
	assert := assert.New(ts.T())
	//require := require.New(ts.T())

	assert.NotNil(Register("db", InfoLevel))
	level, err := GetLevel("db")
	assert.Nil(err)
	assert.Equal(InfoLevel, level)
	assert.NotNil(Register("api", PanicLevel))
	level, err = GetLevel("api")
	assert.Nil(err)
	assert.Equal(PanicLevel, level)
	assert.NotNil(Register("ctx", ErrorLevel))
	level, err = GetLevel("ctx")
	assert.Nil(err)
	assert.Equal(ErrorLevel, level)
}

func (ts *LogTestSuite) TestOutput() {
	assert := assert.New(ts.T())

	buf := new(bytes.Buffer)
	msg := "hello world!"
	ml := Register("db", InfoLevel)
	assert.NotNil(ml)
	assert.Equal(true, SetOutput("db", buf))
	assert.Equal(ml, Md("db"))
	Md("db").Info(msg)
	assert.Contains(buf.String(), msg)
}

func (ts *LogTestSuite) TestModule() {
	assert := assert.New(ts.T())
	buf := new(bytes.Buffer)
	msg := "hello world!"
	Register("testtesttest", InfoLevel)

	assert.Equal(true, SetOutput("testtesttest", buf))
	Md("testtesttest").Debug(msg)
	assert.Equal("", buf.String())

	buf.Reset()
	assert.Equal(true, SetOutput("testtesttest", buf))
	assert.True(SetLevel("testtesttest", DebugLevel))
	Md("testtesttest").Debug(msg)
	assert.Contains(buf.String(), msg)
	assert.Contains(buf.String(), "testtesttest")
}

func (ts *LogTestSuite) TestDef() {
	assert := assert.New(ts.T())
	buf := new(bytes.Buffer)
	msg := "hello world!"

	assert.Equal(true, SetDefOutput(buf))
	Debug(msg)
	assert.Equal("", buf.String())

	buf.Reset()
	assert.Equal(true, SetDefOutput(buf))
	assert.True(SetDefLevel(DebugLevel))
	Debug(msg)
	assert.Contains(buf.String(), msg)
	assert.Contains(buf.String(), "default")
}

func (ts *LogTestSuite) TestModuleFields() {
	assert := assert.New(ts.T())
	buf := new(bytes.Buffer)
	msg := "hello world!"
	module := "testtesttest"
	tag := "tagtagtag"
	Register(module, InfoLevel)
	assert.Equal(true, SetOutput(module, buf))

	Md(module).WithFields(Fields{
		"tag": tag,
	}).Info(msg)

	out := buf.String()
	assert.Contains(out, module)
	assert.Contains(out, msg)
	assert.Contains(out, tag)

	buf.Reset()
	Md(module).WithField("tag", tag).Info(msg)
	out = buf.String()
	assert.Contains(out, "module")
	assert.Contains(out, module)
	assert.Contains(out, msg)
	assert.Contains(out, tag)

	buf.Reset()
	Md(module).WithError(errors.New(tag)).Info(msg)
	out = buf.String()
	assert.Contains(out, module)
	assert.Contains(out, msg)
	assert.Contains(out, tag)
	assert.Contains(out, ErrorKey)
}

func (ts *LogTestSuite) TestDefFields() {
	assert := assert.New(ts.T())
	buf := new(bytes.Buffer)
	msg := "hello world!"
	tag := "tagtagtag"
	assert.Equal(true, SetDefOutput(buf))

	WithFields(Fields{
		"tag": tag,
	}).Info(msg)

	out := buf.String()
	assert.Contains(out, "default")
	assert.Contains(out, msg)
	assert.Contains(out, tag)

	buf.Reset()
	WithField("tag", tag).Info(msg)
	out = buf.String()
	assert.Contains(out, "module")
	assert.Contains(out, "default")
	assert.Contains(out, msg)
	assert.Contains(out, tag)

	buf.Reset()
	WithError(errors.New(tag)).Info(msg)
	out = buf.String()
	assert.Contains(out, "default")
	assert.Contains(out, msg)
	assert.Contains(out, tag)
	assert.Contains(out, ErrorKey)
}

func (ts *LogTestSuite) TestModuleLevels() {
	assert := assert.New(ts.T())
	buf := new(bytes.Buffer)
	msg := "hello world!"
	module := "testtesttest"
	EnableLogRunTime(true)
	ml := Register(module, InfoLevel)
	assert.Equal(true, SetOutput(module, buf))
	levels := []Level{DebugLevel, InfoLevel, WarnLevel, WarnLevel, ErrorLevel, DebugLevel}                                    //, FatalLevel, PanicLevel}
	logFuncs := []func(args ...interface{}){ml.Debug, ml.Info, ml.Warn, ml.Warning, ml.Error, ml.Print}                       //, Fatal, Panic}
	logfFuncs := []func(format string, args ...interface{}){ml.Debugf, ml.Infof, ml.Warnf, ml.Warningf, ml.Errorf, ml.Printf} //, Fatal, Panic}
	loglnFuncs := []func(args ...interface{}){ml.Debugln, ml.Infoln, ml.Warnln, ml.Warningln, ml.Errorln, ml.Println}         //, Fatal, Panic}

	_, err := GetLevel("invalid")
	assert.NotNil(err)

	ret := SetLevel("invalid", ErrorLevel)
	assert.Equal(false, ret)

	ret = SetOutput("invalid", nil)
	assert.Equal(false, ret)

	tmp := Md("invalid")
	assert.NotNil(tmp)

	for idx, level := range levels {
		buf.Reset()
		SetLevel(module, level)
		lfunc := logFuncs[idx]
		lfunc(msg)
		out := buf.String()
		assert.Contains(out, module)
		assert.Contains(out, msg)

		// for the format log
		buf.Reset()
		SetLevel(module, level)
		lffunc := logfFuncs[idx]
		lffunc("%v", msg)
		out = buf.String()
		assert.Contains(out, module)
		assert.Contains(out, msg)

		// for the ln log
		buf.Reset()
		SetLevel(module, level)
		lnfunc := loglnFuncs[idx]
		lnfunc(msg)
		out = buf.String()
		assert.Contains(out, module)
		assert.Contains(out, msg)
	}
}

func (ts *LogTestSuite) TestDefLevels() {
	assert := assert.New(ts.T())
	buf := new(bytes.Buffer)
	msg := "hello world!"
	assert.Equal(true, SetDefOutput(buf))
	levels := []Level{DebugLevel, InfoLevel, WarnLevel, WarnLevel, ErrorLevel, DebugLevel}                  //, FatalLevel, PanicLevel}
	logFuncs := []func(args ...interface{}){Debug, Info, Warn, Warning, Error, Print}                       //, Fatal, Panic}
	logfFuncs := []func(format string, args ...interface{}){Debugf, Infof, Warnf, Warningf, Errorf, Printf} //, Fatal, Panic}
	loglnFuncs := []func(args ...interface{}){Debugln, Infoln, Warnln, Warningln, Errorln, Println}         //, Fatal, Panic}

	for idx, level := range levels {
		buf.Reset()
		SetDefLevel(level)
		lfunc := logFuncs[idx]
		lfunc(msg)
		out := buf.String()
		assert.Contains(out, "default")
		assert.Contains(out, msg)

		// for the format log
		buf.Reset()
		SetDefLevel(level)
		lffunc := logfFuncs[idx]
		lffunc("%v", msg)
		out = buf.String()
		assert.Contains(out, "default")
		assert.Contains(out, msg)

		// for the ln log
		buf.Reset()
		SetDefLevel(level)
		lnfunc := loglnFuncs[idx]
		lnfunc(msg)
		out = buf.String()
		assert.Contains(out, "default")
		assert.Contains(out, msg)
	}

}

func (ts *LogTestSuite) TestPanic() {
	assert := assert.New(ts.T())
	buf := new(bytes.Buffer)
	msg := "hello world!"
	assert.Equal(true, SetDefOutput(buf))

	defer func() {
		if r := recover(); r != nil {
			out := buf.String()
			assert.Contains(out, "default")
			assert.Contains(out, msg)
		}
	}()

	buf.Reset()
	Panic(msg)
}

func (ts *LogTestSuite) TestPanicf() {
	assert := assert.New(ts.T())
	buf := new(bytes.Buffer)
	msg := "hello world!"
	assert.Equal(true, SetDefOutput(buf))

	defer func() {
		if r := recover(); r != nil {
			out := buf.String()
			assert.Contains(out, "default")
			assert.Contains(out, msg)
		}
	}()

	buf.Reset()
	Panicf("%v", msg)
}

func (ts *LogTestSuite) TestPanicln() {
	assert := assert.New(ts.T())
	buf := new(bytes.Buffer)
	msg := "hello world!"
	assert.Equal(true, SetDefOutput(buf))

	defer func() {
		if r := recover(); r != nil {
			out := buf.String()
			assert.Contains(out, "default")
			assert.Contains(out, msg)
		}
	}()

	buf.Reset()
	Panicln(msg)
}

func (ts *LogTestSuite) TestModuleHighLevels() {
	assert := assert.New(ts.T())
	buf := new(bytes.Buffer)
	msg := "hello world!"
	module := "testtesttest"
	EnableLogRunTime(true)
	ml := Register(module, InfoLevel)
	assert.Equal(true, SetOutput(module, buf))
	levels := []Level{InfoLevel, WarnLevel, ErrorLevel, ErrorLevel, FatalLevel, ErrorLevel}                                   //, FatalLevel, PanicLevel}
	logFuncs := []func(args ...interface{}){ml.Debug, ml.Info, ml.Warn, ml.Warning, ml.Error, ml.Print}                       //, Fatal, Panic}
	logfFuncs := []func(format string, args ...interface{}){ml.Debugf, ml.Infof, ml.Warnf, ml.Warningf, ml.Errorf, ml.Printf} //, Fatal, Panic}
	loglnFuncs := []func(args ...interface{}){ml.Debugln, ml.Infoln, ml.Warnln, ml.Warningln, ml.Errorln, ml.Println}         //, Fatal, Panic}

	_, err := GetLevel("invalid")
	assert.NotNil(err)

	ret := SetLevel("invalid", ErrorLevel)
	assert.Equal(false, ret)

	for idx, level := range levels {
		buf.Reset()
		SetLevel(module, level)
		lfunc := logFuncs[idx]
		lfunc(msg)
		out := buf.String()
		assert.Equal("", out)

		// for the format log
		buf.Reset()
		SetLevel(module, level)
		lffunc := logfFuncs[idx]
		lffunc("%v", msg)
		out = buf.String()
		assert.Equal("", out)

		// for the ln log
		buf.Reset()
		SetLevel(module, level)
		lnfunc := loglnFuncs[idx]
		lnfunc(msg)
		out = buf.String()
		assert.Equal("", out)
	}
}

func TestTestSuite(t *testing.T) {
	suite.Run(t, new(LogTestSuite))
}
