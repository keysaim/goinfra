package log

import (
	"time"
)

type TranslogHandler struct {
	Handler *FluentHandler
}

func NewTranslogHandler(host string, port int, tag string) (*TranslogHandler, error) {
	handler, err := NewFluentHandler(host, port, tag)
	if err != nil {
		return nil, err
	}

	th := new(TranslogHandler)
	th.Handler = handler

	return th, nil
}

func (tr *TranslogHandler) Write(data []byte) (n int, err error) {
	msg := make(map[string]interface{})
	msg["msg"] = data
	entry := &LogEntry{Time: time.Now(), msg: &msg}

	if err := tr.Handler.Fire(entry); err != nil {
		return 0, err
	}
	return len(data), nil
}
