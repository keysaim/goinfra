package task

import (
	"math/rand"
)

type Result struct {
	Value interface{}
	Err   error
	T     *Task
}

type TaskFuncType func(...interface{}) (interface{}, error)

type Task struct {
	id      uint32
	args    []interface{}
	do      TaskFuncType
	resChan chan<- *Result
	res     *Result
}

func NewTask(resChan chan<- *Result, taskFunc TaskFuncType, args ...interface{}) *Task {
	return &Task{
		id:      rand.Uint32(),
		args:    args,
		do:      taskFunc,
		resChan: resChan,
	}
}

func (t *Task) run() error {
	val, err := t.do(t.args...)
	res := &Result{val, err, t}
	t.res = res
	if t.resChan != nil {
		t.resChan <- res
	}
	return err
}
