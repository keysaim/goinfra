package relay

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"net"
	"strings"
	"time"

	"gitlab.com/keysaim/goinfra/log"
)

var ErrRelayBindInUse error = errors.New("Relay bind address already in use")

type RelayParam struct {
	Mode    string
	LclPort string
	RmtPort string
	RmtIp   string
}

type SingleRelay struct {
	srcrp *RelayPort
	sikrp *RelayPort

	resChan chan error
	ctx     context.Context
	cancel  context.CancelFunc
}

func NewSingleRelay(pctx context.Context, srcParam *RelayParam, sikParam *RelayParam) *SingleRelay {

	sr := &SingleRelay{
		srcrp:   NewRelayPort(pctx, srcParam),
		sikrp:   NewRelayPort(pctx, sikParam),
		resChan: make(chan error, 1),
	}
	sr.ctx, sr.cancel = context.WithCancel(pctx)
	return sr
}

func (sr *SingleRelay) AddSrcFilter(f RelayFilter) {

	sr.srcrp.AddFilter(f)
}

func (sr *SingleRelay) AddSikFilter(f RelayFilter) {

	sr.sikrp.AddFilter(f)
}

func (sr *SingleRelay) Start() error {

	// make the SingleRelay dual directions
	sr.srcrp.AddWriter(sr.sikrp)
	sr.sikrp.AddWriter(sr.srcrp)

	err := sr.sikrp.Start()
	if err != nil {
		log.Error("Start sink RelayPort failed: ", err)
		return err
	}

	err = sr.srcrp.Start()
	if err != nil {
		log.Error("Start source RelayPort failed: ", err)
		return err
	}

	go func() {
		log.Infof("SingleRelay check source sink loop started")
		var err error

		defer func() {
			sr.resChan <- err
		}()

		for {
			select {
			case <-sr.ctx.Done():
				return
			case err = <-sr.srcrp.Done():
				sr.Stop()
				return
			case err = <-sr.sikrp.Done():
				sr.Stop()
				return
			}
		}
	}()

	return nil
}

func (sr *SingleRelay) Stop() {

	sr.srcrp.Stop()
	sr.sikrp.Stop()
	sr.cancel()
}

func (sr *SingleRelay) Done() <-chan error {

	return sr.resChan
}

type RelayWriter interface {
	Write(data []byte) error
}

type RelayFilter interface {
	// The filter process the data and may produce more datas
	Filter(data [][]byte) ([][]byte, error)
}

type RelayPort struct {
	id           string
	param        *RelayParam
	conn         net.Conn
	connExitChan chan interface{}
	recvBuf      []byte
	resChan      chan error
	rmtAddr      *net.UDPAddr

	wl []RelayWriter
	fl []RelayFilter

	ctx    context.Context
	cancel context.CancelFunc
}

func NewRelayPort(pctx context.Context, param *RelayParam) *RelayPort {

	rp := &RelayPort{
		id:           fmt.Sprintf("%s-%d", param.Mode, 100000+rand.Intn(100000)),
		param:        param,
		connExitChan: make(chan interface{}),
		recvBuf:      make([]byte, 10240),
		resChan:      make(chan error, 1),
	}
	log.Infof("Create RelayPort: %s with param: %v", rp.id, param)
	rp.ctx, rp.cancel = context.WithCancel(pctx)
	return rp
}

func (rp *RelayPort) Id() string {

	return rp.id
}

func (rp *RelayPort) AddWriter(w RelayWriter) {

	rp.wl = append(rp.wl, w)
}

func (rp *RelayPort) AddFilter(f RelayFilter) {

	rp.fl = append(rp.fl, f)
}

func (rp *RelayPort) Start() error {

	var err error

	switch rp.param.Mode {
	case "tcp_active":
		err = rp.startTcpActive()
	case "tcp_passive":
		err = rp.startTcpPassive()
	case "udp":
		err = rp.startUdp()
	default:
		err = errors.New("Unsupported mode: " + rp.param.Mode)
		log.Error(err)
	}
	if err != nil {
		rp.resChan <- err
	}
	return err
}

func (rp *RelayPort) Stop() {

	rp.cancel()
}

func (rp *RelayPort) Done() <-chan error {
	return rp.resChan
}

func (rp *RelayPort) IsReady() bool {
	return rp.conn != nil
}

func (rp *RelayPort) writeConn(conn net.Conn, data []byte) (int, error) {

	if rp.param.Mode == "udp" {
		uc := conn.(*net.UDPConn)
		return uc.WriteToUDP(data, rp.rmtAddr)
	} else {
		return conn.Write(data)
	}
}

func (rp *RelayPort) Write(data []byte) error {

	conn := rp.conn
	if conn == nil {
		log.Debugf("The RelayPort %s is not ready, drop data len: %d", rp.id, len(data))
		return nil
	}

	for {
		n, err := rp.writeConn(conn, data)
		if err != nil {
			log.Errorf("RelayPort %s write error: %s", rp.id, err.Error())
			rp.connExitChan <- struct{}{}
			return err
		}
		if n >= len(data) {
			break
		}
		data = data[n:]
	}
	return nil
}

func (rp *RelayPort) startTcpActive() error {

	var err error
	if len(rp.param.RmtIp) == 0 {
		err := errors.New("The rmtIp must be set for tcp_active")
		log.Error(err)
		return err
	}
	if len(rp.param.RmtPort) == 0 {
		err := errors.New("The rmtPort must be set for tcp_active")
		log.Error(err)
		return err
	}

	log.Infof("RelayPort %s start tcp_active: %v", rp.id, rp.param)

	var ltcpAddr *net.TCPAddr
	if len(rp.param.LclPort) != 0 {
		addr := ":" + rp.param.LclPort
		ltcpAddr, err = net.ResolveTCPAddr("tcp4", addr)
		if err != nil {
			log.Errorf("RelayPort %s resolve local tcp addr failed, addr: %s, error type: %T, error: %s",
				rp.id, addr, err, err.Error())
			return err
		}

	}
	addr := rp.param.RmtIp + ":" + rp.param.RmtPort
	rtcpAddr, err := net.ResolveTCPAddr("tcp4", addr)
	if err != nil {
		log.Errorf("RelayPort %s resolve remote tcp addr failed, addr: %s, error type: %T, error: %s",
			rp.id, addr, err, err.Error())
		return err
	}

	go func() {
		var conn net.Conn
		var err error

		defer func() {
			if conn != nil {
				conn.Close()
			}
			rp.resChan <- err
			log.Infof("The RelayPort %s exit", rp.id)
		}()

		log.Debugf("RelayPort %s tcp_active loop started", rp.id)

		for rp.ctx.Err() == nil {
			conn, err = net.DialTCP("tcp", ltcpAddr, rtcpAddr)

			if err == nil {
				rp.onNewConn(conn)
			} else {
				conn = nil
				if strings.Index(err.Error(), "bind: address already in use") > 0 {
					log.Errorf("RelayPort %s DialTCP failed, err: %s", rp.id, err.Error())
					err = ErrRelayBindInUse
					return
				}
				log.Infof("RelayPort %s cannot connect to %s, error: %s, retry after 1s", rp.id, addr, err.Error())
				time.Sleep(time.Duration(1 * time.Second))
			}
		}
	}()

	return nil
}

func (rp *RelayPort) startTcpPassive() error {

	if len(rp.param.LclPort) == 0 {
		err := errors.New("The local port must be set for the tcp_passive")
		log.Error(err)
		return err
	}

	log.Infof("RelayPort %s start tcp_passive: %v", rp.id, rp.param)

	listen, err := net.Listen("tcp", ":"+rp.param.LclPort)
	if err != nil {
		log.Errorf("Failed to listen port: %s, error: %s", rp.param.LclPort, err.Error())
		return ErrRelayBindInUse
	}
	log.Infof("RelayPort %s listen on port: %s", rp.id, rp.param.LclPort)

	go func() {
		var err error
		var conn net.Conn

		defer func() {
			listen.Close()
			if conn != nil {
				conn.Close()
			}
			log.Infof("The RelayPort %s set result", rp.id)
			rp.resChan <- err
			log.Infof("The RelayPort %s exit", rp.id)
		}()
		go func() {
			log.Infof("RelayPort %s Accept watch dog start", rp.id)
			ctx := rp.ctx
			for {
				select {
				case <-ctx.Done():
					listen.Close()
					return
				}
			}
			log.Infof("RelayPort %s Accept watch dog exit", rp.id)
		}()

		log.Debugf("RelayPort %s loop started", rp.id)

		for rp.ctx.Err() == nil {
			conn, err = listen.Accept()
			if err != nil {
				conn = nil
				log.Errorf("RelayPort %s accept error: %s", rp.id, err.Error())
				return
			}
			rp.onNewConn(conn)
		}
	}()

	return nil
}

func (rp *RelayPort) startUdp() error {

	log.Infof("RelayPort %s start udp: %v", rp.id, rp.param)

	if rp.param.LclPort == "" {
		log.Errorf("Invalid LclPort in param: %v", rp.param)
		return errors.New("Invalid LclPort")
	}
	if rp.param.RmtIp == "" {
		log.Errorf("Invalid RmtIp in param: %v", rp.param)
		return errors.New("Invalid RmtIp")
	}
	if rp.param.RmtPort == "" {
		log.Errorf("Invalid RmtPort in param: %v", rp.param)
		return errors.New("Invalid RmtPort")
	}
	localAddr, err := net.ResolveUDPAddr("udp", fmt.Sprintf(":%s", rp.param.LclPort))
	if err != nil {
		log.Error(err)
		return err
	}

	rp.rmtAddr, err = net.ResolveUDPAddr("udp", fmt.Sprintf("%s:%s", rp.param.RmtIp, rp.param.RmtPort))
	if err != nil {
		log.Error(err)
		return err
	}

	conn, err := net.ListenUDP("udp", localAddr)
	if err != nil {
		log.Error(err)
		return err
	}
	//data := make([]byte, 1024)
	//for {
	//n, remoteAddr, err := listener.ReadFromUDP(data)
	//if err != nil {
	//fmt.Printf("error during read: %s", err)
	//}
	//fmt.Printf("<%s> %s\n", remoteAddr, data[:n])
	//_, err = listener.WriteToUDP([]byte("world"), remoteAddr)
	//if err != nil {
	//fmt.Printf(err.Error())
	//}
	//}

	go func() {
		rp.onNewConn(conn)
		rp.resChan <- nil
	}()

	return nil
}

func (rp *RelayPort) onNewConn(conn net.Conn) {

	log.Debugf("RelayPort %s got one new conn: %v", rp.id, conn)

	rp.conn = conn
	go rp.handleConn(conn)
	for {
		select {
		case <-rp.ctx.Done():
			log.Infof("The RelayPort %s exit when canceled", rp.id)
			return
		case <-rp.connExitChan:
			log.Infof("The last RelayPort %s conn %v exit, will try to start one new", rp.id, conn)
			conn.Close()
			rp.conn = nil
			return
		}
	}
}

func (rp *RelayPort) handleConn(conn net.Conn) {

	defer func() {
		rp.connExitChan <- struct{}{}
	}()

	log.Debugf("RelayPort %s handle conn read", rp.id)

	for rp.ctx.Err() == nil {
		len, err := conn.Read(rp.recvBuf)
		if err != nil {
			log.Errorf("RelayPort %s read error: %s", rp.id, err.Error())
			return
		}

		err = rp.send(rp.recvBuf[:len])
		if err != nil {
			log.Errorf("RelayPort %s send error: %s", rp.id, err.Error())
			return
		}
	}
}

func (rp *RelayPort) send(data []byte) error {

	var err error

	log.Debugf("RelayPort %s try to send data: %d", rp.id, len(data))

	fl := rp.fl
	dl := [][]byte{data}
	for _, f := range fl {
		dl, err = f.Filter(dl)
		if err != nil {
			return err
		}
	}

	wl := rp.wl
	for _, w := range wl {
		for _, d := range dl {
			err = w.Write(d)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
