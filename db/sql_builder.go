package db

import (
	"fmt"
	"strings"

	"gitlab.com/keysaim/goinfra/log"
)

type JoinType int

const (
	INNER_JOIN JoinType = iota
	LEFT_JOIN
	LEFT_DIFF_JOIN
)

type SqlBuilder struct {
}

func (b *SqlBuilder) Insert(tbl string, cols []string, values []string) string {

	return fmt.Sprintf("INSERT INTO `%s` (`%s`) VALUES ('%s')", tbl, strings.Join(cols, "`, `"), strings.Join(values, ", '"))
}

func (b *SqlBuilder) InsertNamed(tbl string, cols []string, fields []string) string {

	if len(fields) == 0 {
		fields = cols
	}
	return fmt.Sprintf("INSERT INTO `%s` (`%s`) VALUES (:%s)", tbl, strings.Join(cols, "`, `"), strings.Join(fields, ", :"))
}

func (b *SqlBuilder) Update(tbl string, set map[string]string, where map[string]string) string {

	sql := fmt.Sprintf("UPDATE `%s` SET ", tbl)

	idx := 0
	for col, val := range set {
		if idx > 0 {
			sql += ", "
		}
		if val == "NOW()" {
			sql += fmt.Sprintf("`%s`=NOW()", col)
		} else {
			sql += fmt.Sprintf("`%s`='%s'", col, val)
		}
		idx += 1
	}

	if len(where) > 0 {
		sql += " " + b.Where(where, true)
	}
	return sql
}

func (b *SqlBuilder) UpdateNamed(tbl string, cols []string, fields []string, where map[string]string) string {

	sql := fmt.Sprintf("UPDATE `%s` SET ", tbl)

	if len(fields) == 0 {
		fields = cols
	}
	for idx, col := range cols {
		if idx > 0 {
			sql += ", "
		}
		fd := fields[idx]
		sql += fmt.Sprintf("`%s`=:%s", col, fd)
	}

	if len(where) > 0 {
		sql += " " + b.Where(where, true)
	}
	return sql
}

func (b *SqlBuilder) Delete(tbl string, where map[string]string) string {

	sql := fmt.Sprintf("DELETE FROM `%s`", tbl)
	if len(where) > 0 {
		sql += " " + b.Where(where, true)
	}
	return sql
}

func (b *SqlBuilder) Select(tbl string, where map[string]string,
	order []string, offset int, limit int, function []string) string {

	var sql string
	if function != nil {
		if len(function) < 2 {
			log.Errorf("Invalid function: %v", function)
			return ""
		}
		name := function[0]
		field := function[1]
		sql = fmt.Sprintf("SELECT %s(%s) FROM `%s`", name, field, tbl)
	} else {
		sql = "SELECT * FROM `" + tbl + "`"
	}

	// Where clause
	if len(where) > 0 {
		sql += " " + b.Where(where, true)
	}

	// Order clause
	if len(order) > 0 {
		sql += " " + b.Order(order)
	}

	if limit > 0 {
		sql += " " + b.Limit(limit)
	}
	if offset > 0 {
		sql += " " + b.Offset(offset)
	}
	return sql
}

func (b *SqlBuilder) JoinSelect(jtype JoinType, ltbl string, rtbl string, conds map[string]string,
	lfields []string, rfields []string, where map[string]string, order []string, offset int, limit int) string {

	var jstr string
	if jtype == INNER_JOIN {
		jstr = "INNER"
	} else {
		jstr = "LEFT"
	}

	lfmt := ""
	rfmt := ""
	if lfields == nil {
		// use all
		lfmt = "%[1]s.*"
	} else if len(lfields) > 0 {
		// If it's an empty string slice, skip left fields
		ts := make([]string, len(lfields))
		for i := range lfields {
			n := lfields[i]
			if n[0] == "^"[0] {
				ts[i] = n[1:]
			} else {
				ts[i] = "%[1]s." + n
			}
		}
		lfmt = strings.Join(ts, ",")
	}
	if rfields == nil {
		// use all
		rfmt = "%[2]s.*"
	} else if len(rfields) > 0 {
		// If it's an empty string slice, skip left fields
		ts := make([]string, len(rfields))
		for i := range rfields {
			n := rfields[i]
			if n[0] == "^"[0] {
				ts[i] = n[1:]
			} else {
				ts[i] = "%[2]s." + n
			}
		}
		rfmt = strings.Join(ts, ",")
	}
	afmt := lfmt
	if rfmt != "" {
		afmt += "," + rfmt
	}
	fmtstr := "SELECT " + afmt + " FROM `%[1]s` %[3]s JOIN `%[2]s` ON"

	sql := fmt.Sprintf(fmtstr, ltbl, rtbl, jstr)
	//sql := fmt.Sprintf("SELECT %s.* FROM %s %s JOIN %s ON", ltbl, ltbl, jstr, rtbl)
	sql += " " + b.JoinConds(ltbl, rtbl, conds)

	if len(where) > 0 {
		sql += " " + b.JoinWhere(ltbl, rtbl, where)
	}

	if jtype == LEFT_DIFF_JOIN {
		s := ""
		for _, rn := range conds {
			if s != "" {
				s += " AND "
			}
			s += fmt.Sprintf("%s.%s is NULL", rtbl, rn)
		}
		if len(where) > 0 {
			sql += " AND " + s
		} else {
			sql += " WHERE " + s
		}
	}

	if len(order) > 0 {
		sql += " " + b.JoinOrder(ltbl, order)
	}

	if limit > 0 {
		sql += " " + b.Limit(limit)
	}
	if offset > 0 {
		sql += " " + b.Offset(offset)
	}
	return sql
}

func (b *SqlBuilder) JoinConds(ltbl string, rtbl string, conds map[string]string) string {

	sql := ""
	idx := 0
	for ln, rn := range conds {

		if idx > 0 {
			sql += " AND "
		}
		sql += fmt.Sprintf("%s.%s=%s.%s", ltbl, ln, rtbl, rn)
		idx += 1
	}
	return sql
}

func (b *SqlBuilder) JoinWhere(ltbl, rtbl string, where map[string]string) string {

	nwhere := make(map[string]string)
	for k, v := range where {
		if k[0] == "."[0] {
			nwhere[rtbl+k] = v
		} else {
			nwhere[ltbl+"."+k] = v
		}
	}

	return b.Where(nwhere, false)
}

func (b *SqlBuilder) JoinOrder(tbl string, order []string) string {

	norder := make([]string, len(order))
	for i, v := range order {
		if v == "" {
			continue
		}
		s := v[0]
		if s == "+"[0] || s == "-"[0] {
			v = fmt.Sprintf("%s%s.%s", s, tbl, v)
		} else {
			v = fmt.Sprintf("%s.%s", tbl, v)
		}
		norder[i] = v
	}
	return b.Order(norder)
}

func GetSqlOperators() map[string]map[string]string {
	return map[string]map[string]string{
		"logical": map[string]string{
			"":  "by default, it will use the AND logic",
			"!": "NOT logic",
			"|": "OR logic",
		},
		"comparable": map[string]string{
			"":   "by default, it will use '='",
			"^":  "for sql '<>', means not equal",
			">":  "for sql '>'",
			">=": "for sql '>='",
			"<":  "for sql '<'",
			"<=": "for sql '<='",
			"~":  "for sql 'LIKE', e.g. '~a%'",
			"[":  "for sql 'BETWEEN', the value is 'val1,val2'",
			"@":  "for sql 'IN', the value is 'val1,val2...valN'",
		},
	}
}

// WHERE <cond>...
// <cond>=<operator><value>, and the <operator> is as above
// For the logic flag, it can have one more calc flag
func (b *SqlBuilder) Where(where map[string]string, quote bool) string {

	return fmt.Sprintf("WHERE %s", b.WhereConds(where, quote))
}

func (b *SqlBuilder) WhereConds(where map[string]string, quote bool) string {

	sql := ""
	idx := 0
	for k, v := range where {
		ov := v
		var lo string
		if len(v) > 1 {
			lo = v[:1]
		}

		// Handle the logical operators
		blo := true
		if idx > 0 {
			sql += " "
		}
		switch lo {
		case "!":
			if idx > 0 {
				sql += "AND NOT "
			} else {
				sql += "NOT "
			}
		case "|":
			if idx > 0 {
				// Will skip if it's the first one
				sql += "OR "
			} else {
				blo = false
				v = v[1:]
			}
		case "\\":
			v = v[1:]
			blo = false
		default:
			blo = false
		}
		if blo {
			v = v[1:] // skip the logical operator
		}
		if idx > 0 {
			if !blo {
				sql += "AND "
			}
		}

		var co string
		if len(v) > 1 {
			co = v[:1]
		}
		bco := true
		off := 1
		switch co {
		case "^":
			co = "<>"
		case ">":
			if len(v) > 2 {
				t := v[1:2]
				if t == "=" {
					co = ">="
					off = 2
				}
			}
		case "<":
			if len(v) > 2 {
				t := v[1:2]
				if t == "=" {
					co = "<="
					off = 2
				}
			}
		case "~":
			co = "LIKE"
		case "[":
			co = "BETWEEN"
		case "@":
			co = "IN"
		case "-":
			bco = false
			if len(v) > 2 {
				t := v[1:2]
				if t == ">" {
					co = "IS"
					off = 2
					bco = true
				}
			}
		case "\\":
			v = v[1:]
			bco = false
		default:
			bco = false
		}
		if bco {
			v = v[off:]
		}

		vq := "%s"
		if quote {
			vq = "`%s`"
		}

		if bco {
			if co == "BETWEEN" {
				vl := strings.Split(v, ",")
				if len(vl) < 2 {
					log.Errorf("Invalid BETWEEN cond: %s", ov)
					return ""
				}

				fstr := fmt.Sprintf("%s BETWEEN '%%s' AND '%%s'", vq)
				sql += fmt.Sprintf(fstr, k, vl[0], vl[1])
			} else if co == "IN" {
				vl := strings.Split(v, ",")
				vs := strings.Join(vl, "', '")
				is := "('" + vs + "')"
				fstr := fmt.Sprintf("%s IN %%s", vq)
				sql += fmt.Sprintf(fstr, k, is)
			} else if co == "IS" {
				fstr := fmt.Sprintf("%s IS %%s", vq)
				sql += fmt.Sprintf(fstr, k, v)
			} else {
				fstr := fmt.Sprintf("%s %s '%%s'", vq, co)
				sql += fmt.Sprintf(fstr, k, v)
			}
		} else {
			fstr := fmt.Sprintf("%s='%%s'", vq)
			sql += fmt.Sprintf(fstr, k, v)
		}
		idx += 1
	}
	return sql
}

func (b *SqlBuilder) Order(order []string) string {
	sql := "ORDER BY"
	for i, o := range order {
		if len(o) == 0 {
			continue
		}
		t := o[0]
		var s string
		if t == "+"[0] {
			s = " ASC"
			o = o[1:]
		} else if t == "-"[0] {
			s = " DESC"
			o = o[1:]
		}
		if i > 0 {
			sql += ", "
		} else {
			sql += " "
		}
		sql += "`" + o + "`"
		if s != "" {
			sql += s
		}
	}
	return sql
}

func (b *SqlBuilder) Offset(offset int) string {
	return fmt.Sprintf("OFFSET %d", offset)
}

func (b *SqlBuilder) Limit(limit int) string {
	return fmt.Sprintf("LIMIT %d", limit)
}
