package db

import (
	"database/sql"
	"errors"
	"fmt"
	"strconv"

	"github.com/jmoiron/sqlx"
	"gitlab.com/keysaim/goinfra/log"
)

type SqlParams struct {
	Jtype   JoinType
	Rname   string
	Conds   map[string]string
	Lfields []string
	Rfields []string
	Where   map[string]string
	Order   []string
	Offset  int
	Limit   int
}

func NewSqlParams() *SqlParams {
	return &SqlParams{
		Jtype:   INNER_JOIN,
		Rfields: []string{},
	}
}

func NewJoinParams(rname string, conds map[string]string, where map[string]string) *SqlParams {
	return &SqlParams{
		Jtype:   INNER_JOIN,
		Rname:   rname,
		Conds:   conds,
		Rfields: []string{},
		Where:   where,
	}
}

// New full join to get both the left and right fields
func NewFjoinParams(rname string, conds map[string]string, where map[string]string) *SqlParams {
	return &SqlParams{
		Jtype: INNER_JOIN,
		Rname: rname,
		Conds: conds,
		Where: where,
	}
}

type Handler interface {
	New() Model
	Mnew(len int) interface{} // create a list of Model
	Add(m Model) error
	Madd(ml interface{}) error
	Get(id int64) (Model, error)
	Find(where map[string]string, order []string, offset int, limit int) (interface{}, error)
	FindOne(where map[string]string, order []string, offset int) (Model, error)
	All() (interface{}, error)
	Join(p *SqlParams) (interface{}, error)
	JoinCount(p *SqlParams) (int, error)
	JoinModels(ml interface{}, p *SqlParams) error

	Func(where map[string]string, order []string, offset int, limit int, function []string) (string, error)
	Count(where map[string]string) (int, error)
	Put(id int64, m Model) error
	Mput(ml interface{}) error
	Del(id int64) error
	Update(set map[string]string, where map[string]string) error
	Remove(where map[string]string) error
}

type JoinParams struct {
	Type  JoinType
	Rname string
	Conds map[string]string
}

var ErrNotImplement = errors.New("Method not implement")

type BaseHandler struct {
	db      *sqlx.DB
	Builder *SqlBuilder
	name    string
	tblName string

	NewFunc    func() Model
	MnewFunc   func(len int) interface{}
	SliceFunc  func(ml interface{}) (interface{}, bool)
	ModelsFunc func(ml interface{}) ([]Model, bool)
}

func NewHandler(db *sqlx.DB, name string) *BaseHandler {

	tblName := TableName(name)
	if tblName == "" {
		log.Fatalf("Failed to find the table name for name: %s", name)
	}
	return &BaseHandler{
		db:      db,
		Builder: &SqlBuilder{},
		name:    name,
		tblName: tblName,
	}
}

func (h *BaseHandler) SetDB(db *sqlx.DB) {
	h.db = db
}

func (h *BaseHandler) AddModel(model Model, tbl string, cols []string, fields []string) error {

	sql := h.Builder.InsertNamed(tbl, cols, fields)
	log.Debug(sql)
	res, err := h.db.NamedExec(sql, model)
	if err != nil {
		log.Error(err)
		return err
	}

	id, err := res.LastInsertId()
	if err != nil {
		log.Error(err)
		return err
	}
	model.SetId(id)
	return nil
}

func (h *BaseHandler) GetModel(model Model, tbl string, id int64) error {

	return h.FindModel(model, tbl, map[string]string{"id": fmt.Sprintf("%d", id)}, nil, 0)
}

func (h *BaseHandler) FindModel(model Model, tbl string,
	where map[string]string, order []string, offset int) error {

	sqlstr := h.Builder.Select(tbl, where, order, offset, 1, nil)
	log.Debug(sqlstr)
	err := h.db.Get(model, sqlstr)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Error(err)
		}
		return err
	}

	return nil
}

func (h *BaseHandler) FindModels(models interface{}, tbl string,
	where map[string]string, order []string, offset int, limit int) error {

	sql := h.Builder.Select(tbl, where, order, offset, limit, nil)
	log.Debug(sql)
	err := h.db.Select(models, sql)
	if err != nil {
		log.Error(err)
		return err
	}

	return nil
}

func (h *BaseHandler) RunFunc(tbl string,
	where map[string]string, order []string, offset int, limit int, function []string) (value string, err error) {

	sql := h.Builder.Select(tbl, where, order, offset, limit, function)
	log.Debug(sql)
	row := h.db.QueryRow(sql)
	if err != nil {
		log.Error(err)
		return
	}
	err = row.Scan(&value)
	if err != nil {
		log.Error(err)
		return
	}

	return
}

func (h *BaseHandler) JoinModels(models interface{}, p *SqlParams) error {

	rtbl := TableName(p.Rname)
	if rtbl == "" {
		err := errors.New(fmt.Sprintf("Invalid right model name: %s", p.Rname))
		log.Error(err)
		return err
	}
	sql := h.Builder.JoinSelect(p.Jtype, h.tblName, rtbl, p.Conds, p.Lfields,
		p.Rfields, p.Where, p.Order, p.Offset, p.Limit)
	log.Debug(sql)
	err := h.db.Select(models, sql)
	if err != nil {
		log.Error(err)
		return err
	}

	return nil
}

func (h *BaseHandler) UpdateModel(model Model, tbl string, id int64, cols []string, fields []string) error {

	return h.UpdateModelOn(model, tbl, cols, fields, map[string]string{"id": fmt.Sprintf("%d", id)})
}

func (h *BaseHandler) UpdateModelOn(model Model, tbl string, cols []string, fields []string, where map[string]string) error {

	sql := h.Builder.UpdateNamed(tbl, cols, fields, where)
	log.Debug(sql)
	_, err := h.db.NamedExec(sql, model)
	if err != nil {
		log.Error(err)
		return err
	}

	return nil
}

func (h *BaseHandler) Update(set map[string]string, where map[string]string) error {

	sql := h.Builder.Update(h.tblName, set, where)
	log.Debug(sql)
	_, err := h.db.Exec(sql)
	if err != nil {
		log.Error(err)
		return err
	}

	return nil
}

func (h *BaseHandler) Remove(where map[string]string) error {

	sql := h.Builder.Delete(h.tblName, where)
	log.Debug(sql)
	_, err := h.db.Exec(sql)
	if err != nil {
		log.Error(err)
		return err
	}
	return nil
}

func (h *BaseHandler) DeleteModel(tbl string, id int64) error {

	return h.Remove(map[string]string{"id": fmt.Sprintf("%d", id)})
}

func (h *BaseHandler) Add(m Model) error {

	cols := TableFields(h.name, DB_OP_ADD)
	return h.AddModel(m, h.tblName, cols, nil)
}

func (h *BaseHandler) Madd(ml interface{}) error {

	ll, ok := h.ModelsFunc(ml)
	if !ok {
		err := errors.New(fmt.Sprintf("Invalid Model slice type: %T", ml))
		log.Error(err)
		return err
	}
	for i := range ll {
		err := h.Add(ll[i])
		if err != nil {
			log.Error(err)
			return err
		}
	}
	return nil
}

func (h *BaseHandler) Get(id int64) (Model, error) {

	m := h.NewFunc()
	err := h.GetModel(m, h.tblName, id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	return m, err
}

func (h *BaseHandler) Find(where map[string]string, order []string,
	offset int, limit int) (interface{}, error) {

	ml := h.MnewFunc(0)
	if err := h.FindModels(ml, h.tblName, where, order, offset, limit); err != nil {
		log.Error(err)
		return nil, err
	}
	s, ok := h.SliceFunc(ml)
	if !ok {
		err := errors.New(fmt.Sprintf("Invalid Model slice type: %T", ml))
		log.Error(err)
		return nil, err
	}
	return s, nil

}

func (h *BaseHandler) All() (interface{}, error) {

	return h.Find(nil, nil, 0, 0)
}

func (h *BaseHandler) Join(params *SqlParams) (interface{}, error) {

	ml := h.MnewFunc(0)
	params.Rfields = []string{}
	if err := h.JoinModels(ml, params); err != nil {
		log.Error(err)
		return nil, err
	}
	s, ok := h.SliceFunc(ml)
	if !ok {
		err := errors.New(fmt.Sprintf("Invalid Model slice type: %T", ml))
		log.Error(err)
		return nil, err
	}
	return s, nil

}

func (h *BaseHandler) Func(where map[string]string, order []string,
	offset int, limit int, function []string) (string, error) {

	if res, err := h.RunFunc(h.tblName, where, order, offset, limit, function); err != nil {
		log.Error(err)
		return "", err
	} else {
		return res, nil
	}
}

func (h *BaseHandler) Count(where map[string]string) (int, error) {

	function := []string{"count", "*"}
	if res, err := h.RunFunc(h.tblName, where, nil, 0, 0, function); err != nil {
		log.Error(err)
		return 0, err
	} else {
		v, err := strconv.Atoi(res)
		return v, err
	}
}

func (h *BaseHandler) JoinCount(p *SqlParams) (int, error) {

	var cl []int
	p.Lfields = []string{"^count(*)"}
	err := h.JoinModels(&cl, p)
	if err != nil {
		return 0, err
	}
	return cl[0], nil
}

func (h *BaseHandler) FindOne(where map[string]string, order []string,
	offset int) (Model, error) {

	m := h.NewFunc()
	err := h.FindModel(m, h.tblName, where, order, offset)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	return m, err
}

func (h *BaseHandler) Put(id int64, m Model) error {

	cols := TableFields(h.name, DB_OP_PUT)
	return h.UpdateModel(m, h.tblName, id, cols, nil)
}

func (h *BaseHandler) Mput(ml interface{}) error {

	ll, ok := h.ModelsFunc(ml)
	if !ok {
		err := errors.New(fmt.Sprintf("Invalid Model slice type: %T", ml))
		return err
	}
	for i := range ll {
		m := ll[i]
		err := h.Put(m.ID(), m)
		if err != nil {
			return err
		}
	}
	return nil
}

func (h *BaseHandler) Del(id int64) error {

	return h.DeleteModel(h.tblName, id)
}
