package gt03

import (
	"testing"
	"time"

	_ "github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/keysaim/goinfra/iox/buffer"
	"gitlab.com/keysaim/goinfra/log"
)

type PackageTestSuite struct {
	suite.Suite
}

func (ts *PackageTestSuite) SetupSuite() {
	log.EnableLogRunTime(true)
	log.Info("SetupSuite")
}

func (ts *PackageTestSuite) TearDownSuite() {
	log.Info("TeardownSuite")
}

func (ts *PackageTestSuite) TestLoginInfo() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	//real client login info: 7878110108681201436262052020320200003ca10d0a
	raw := [...]byte{0x78, 0x78, 0x11, 0x01, 0x01, 0x23, 0x45, 0x67, 0x89, 0x01, 0x23, 0x45, 0x10, 0x0B, 0x32, 0x00, 0x00, 0x01, 0xA3, 0x67, 0x0D, 0x0A}
	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x01), pkt.ctype)
	assert.Equal(int16(1), pkt.seq)

	lg, ok := pkt.content.(*LoginInfo)
	assert.True(ok)
	assert.Equal(8, len(lg.imei))
	assert.Equal(byte(0x01), lg.imei[0])
	assert.Equal(byte(0x45), lg.imei[7])
	assert.Equal("GT03B", lg.verStr)
	assert.Equal(false, lg.zoneWest)
	assert.Equal("GMT+8:00", lg.zone)

	buf := buffer.NewValueBuf(64)
	_, err = pkt.EncodeRsps(buf)
	assert.Nil(err)

	rsps := [...]byte{0x78, 0x78, 0x0F, 0x01, 0x01, 0x23, 0x45, 0x67, 0x89, 0x01, 0x23, 0x45, 0x10, 0x0B, 0x00, 0x01, 0xF2, 0xE6, 0x0D, 0x0A}
	assert.Equal(rsps[:], buf.Bytes())
}

func (ts *PackageTestSuite) TestGpsInfo() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	raw := [...]byte{0x78, 0x78, 0x19, 0x10, 0x0B, 0x08, 0x1D, 0x11, 0x2E, 0x10,
		0x9C, 0x02, 0x7A, 0xC7, 0xEB, 0x0C, 0x46, 0x58, 0x49, 0x00, 0x14, 0x8F,
		0x00, 0x01, 0x00, 0x03, 0x80, 0x81, 0x0D, 0x0A}
	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	log.Infof("raw len: %d", len(raw))
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x10), pkt.ctype)
	assert.Equal(int16(3), pkt.seq)

	gi, ok := pkt.content.(*GpsInfo)
	assert.True(ok)
	assert.Equal(time.Date(2000+int(0x0B), time.Month(0x08), int(0x1D), int(0x11), int(0x2E), int(0x10), 0, time.UTC), gi.date)
	assert.Equal(byte(9), gi.size)
	assert.Equal(byte(12), gi.num)
	assert.True(23.1117-gi.lat < 0.001)
	assert.True(114.4093-gi.lng < 0.001)
	assert.Equal(byte(0), gi.vel)
	assert.Equal(false, gi.diffPos)
	assert.Equal(true, gi.hasPos)
	assert.Equal(true, gi.latNorth)
	assert.Equal(false, gi.lngWest)

	rsps := buffer.NewValueBuf(64)
	_, err = pkt.EncodeRsps(rsps)
	assert.Nil(err)
	raw2 := [...]byte{0x78, 0x78, 0x05, 0x10, 0x00, 0x03, 0x25, 0x87, 0x0D, 0x0A}
	assert.Equal(raw2[:], rsps.Bytes())
}

func (ts *PackageTestSuite) TestLbsInfo() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	raw := [...]byte{0x78, 0x78, 0x15, 0x11, 0x0B, 0x08, 0x1D, 0x11, 0x2E, 0x10,
		0x01, 0xCC, 0x00, 0x26, 0x6A, 0x00, 0x1D, 0xF1, 0x00, 0x01,
		0x00, 0x18, 0x91, 0x88, 0x0D, 0x0A}
	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	log.Infof("raw len: %d", len(raw))
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x11), pkt.ctype)
	assert.Equal(int16(0x18), pkt.seq)

	li, ok := pkt.content.(*LbsInfo)
	assert.True(ok)
	assert.Equal(time.Date(2000+int(0x0B), time.Month(0x08), int(0x1D), int(0x11), int(0x2E), int(0x10), 0, time.UTC), li.date)
	assert.Equal(int16(0x01CC), li.mcc)
	assert.Equal(byte(0x00), li.mnc)
	assert.Equal(int16(0x266A), li.lac)
	assert.Equal(int32(0x001DF1), li.cell)

	rsps := buffer.NewValueBuf(64)
	_, err = pkt.EncodeRsps(rsps)
	assert.Nil(err)
	raw2 := [...]byte{0x78, 0x78, 0x05, 0x11, 0x00, 0x18, 0xD1, 0x09, 0x0D, 0x0A}
	assert.Equal(raw2[:], rsps.Bytes())
}

func (ts *PackageTestSuite) TestStatusInfo() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	raw := [...]byte{0x78, 0x78, 0x0A, 0x13,
		0x40, 0x06, 0x04, 0x00, 0x01,
		0x00, 0x1F, 0xC4, 0x39, 0x0D, 0x0A}
	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	log.Infof("raw len: %d", len(raw))
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x13), pkt.ctype)
	assert.Equal(int16(0x1F), pkt.seq)

	si, ok := pkt.content.(*StatusInfo)
	assert.True(ok)
	assert.False(si.buzz)
	assert.False(si.fence)
	assert.False(si.charge)
	assert.Equal(GT_WARN_NONE, si.warn)
	assert.True(si.hasPos)
	assert.Equal(byte(0x06), si.vol)
	assert.Equal(GT_GSM_VERY_GOOD, si.gsm)

	rsps := buffer.NewValueBuf(64)
	_, err = pkt.EncodeRsps(rsps)
	assert.Nil(err)
	raw2 := [...]byte{0x78, 0x78, 0x05, 0x13, 0x00, 0x1F, 0x10, 0x0E, 0x0D, 0x0A}
	assert.Equal(raw2[:], rsps.Bytes())
}

func (ts *PackageTestSuite) TestGpsLbsStatusInfo() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	raw := [...]byte{0x78, 0x78, 0x26, 0x16, 0x0B, 0x08, 0x1D, 0x11, 0x2E, 0x10,
		0x9C, 0x02, 0x7A, 0xC7, 0xEB, 0x0C, 0x46, 0x58, 0x49, 0x00, 0x14, 0x8F,
		0x08, 0x01, 0xCC, 0x00, 0x26, 0x6A, 0x00, 0x1D, 0xF1,
		0x40, 0x06, 0x04, 0x00, 0x01, 0x01,
		0x00, 0x1F, 0xC4, 0x39, 0x0D, 0x0A}

	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	log.Infof("raw len: %d", len(raw))
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x16), pkt.ctype)
	assert.Equal(int16(0x1F), pkt.seq)

	ci, ok := pkt.content.(*GpsLbsStatusInfo)
	assert.True(ok)
	assert.Equal(time.Date(2000+int(0x0B), time.Month(0x08), int(0x1D), int(0x11), int(0x2E), int(0x10), 0, time.UTC), ci.date)

	assert.Equal(byte(9), ci.gps.size)
	assert.Equal(byte(12), ci.gps.num)
	assert.True(23.1117-ci.gps.lat < 0.001)
	assert.True(114.4093-ci.gps.lng < 0.001)
	assert.Equal(byte(0), ci.gps.vel)
	assert.Equal(false, ci.gps.diffPos)
	assert.Equal(true, ci.gps.hasPos)
	assert.Equal(true, ci.gps.latNorth)
	assert.Equal(false, ci.gps.lngWest)

	assert.Equal(int16(0x01CC), ci.lbs.mcc)
	assert.Equal(byte(0x00), ci.lbs.mnc)
	assert.Equal(int16(0x266A), ci.lbs.lac)
	assert.Equal(int32(0x001DF1), ci.lbs.cell)

	assert.False(ci.status.buzz)
	assert.False(ci.status.fence)
	assert.False(ci.status.charge)
	assert.Equal(GT_WARN_NONE, ci.status.warn)
	assert.True(ci.status.hasPos)
	assert.Equal(byte(0x06), ci.status.vol)
	assert.Equal(GT_GSM_VERY_GOOD, ci.status.gsm)

	rsps := buffer.NewValueBuf(64)
	ci.cmd = *NewDefCmdInfoWithValues(int32(0x10101010), "test-cmd")
	_, err = pkt.EncodeRsps(rsps)
	assert.Nil(err)

	raw2 := [...]byte{0x78, 0x78, 0x12, 0x16,
		0x0C, 0x10, 0x10, 0x10, 0x10, 0x74, 0x65, 0x73, 0x74, 0x2d, 0x63, 0x6d, 0x64,
		0x00, 0x1F, 0xFC, 0x49, 0x0D, 0x0A}

	assert.Equal(raw2[:], rsps.Bytes())
}

func (ts *PackageTestSuite) TestGpsLbsStatusInfoEng() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	raw := [...]byte{0x78, 0x78, 0x26, 0x16, 0x0B, 0x08, 0x1D, 0x11, 0x2E, 0x10,
		0x9C, 0x02, 0x7A, 0xC7, 0xEB, 0x0C, 0x46, 0x58, 0x49, 0x00, 0x14, 0x8F,
		0x08, 0x01, 0xCC, 0x00, 0x26, 0x6A, 0x00, 0x1D, 0xF1,
		0x40, 0x06, 0x04, 0x00, 0x01, 0x01,
		0x00, 0x1F, 0xC4, 0x39, 0x0D, 0x0A}

	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	log.Infof("raw len: %d", len(raw))
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x16), pkt.ctype)
	assert.Equal(int16(0x1F), pkt.seq)

	ci, ok := pkt.content.(*GpsLbsStatusInfo)
	assert.True(ok)
	assert.Equal(time.Date(2000+int(0x0B), time.Month(0x08), int(0x1D), int(0x11), int(0x2E), int(0x10), 0, time.UTC), ci.date)

	assert.Equal(byte(9), ci.gps.size)
	assert.Equal(byte(12), ci.gps.num)
	assert.True(23.1117-ci.gps.lat < 0.001)
	assert.True(114.4093-ci.gps.lng < 0.001)
	assert.Equal(byte(0), ci.gps.vel)
	assert.Equal(false, ci.gps.diffPos)
	assert.Equal(true, ci.gps.hasPos)
	assert.Equal(true, ci.gps.latNorth)
	assert.Equal(false, ci.gps.lngWest)

	assert.Equal(int16(0x01CC), ci.lbs.mcc)
	assert.Equal(byte(0x00), ci.lbs.mnc)
	assert.Equal(int16(0x266A), ci.lbs.lac)
	assert.Equal(int32(0x001DF1), ci.lbs.cell)

	assert.False(ci.status.buzz)
	assert.False(ci.status.fence)
	assert.False(ci.status.charge)
	assert.Equal(GT_WARN_NONE, ci.status.warn)
	assert.True(ci.status.hasPos)
	assert.Equal(byte(0x06), ci.status.vol)
	assert.Equal(GT_GSM_VERY_GOOD, ci.status.gsm)

	rsps := buffer.NewValueBuf(64)
	ci.status.lang = GT_LANG_ENG
	ci.cmd = *NewDefCmdInfoWithValues(int32(0x10101010), "test-cmd")
	ci.cmd.lang = ci.status.lang
	_, err = pkt.EncodeRsps(rsps)
	assert.Nil(err)

	raw2 := [...]byte{0x78, 0x78, 0x00, 0x13, 0x16,
		0x00, 0x0C, 0x10, 0x10, 0x10, 0x10, 0x74, 0x65, 0x73, 0x74, 0x2d, 0x63, 0x6d, 0x64,
		0x00, 0x1F, 0x67, 0x37, 0x0D, 0x0A}

	assert.Equal(raw2[:], rsps.Bytes())
}

func (ts *PackageTestSuite) TestLbsPhoneInfo() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	raw := [...]byte{0x78, 0x78, 0x22, 0x17,
		0x01, 0xCC, 0x00, 0x26, 0x6A, 0x00, 0x1D, 0xF1,
		0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x05,
		0x00, 0x1F, 0xC4, 0x39, 0x0D, 0x0A}

	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	log.Infof("raw len: %d", len(raw))
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x17), pkt.ctype)
	assert.Equal(int16(0x1F), pkt.seq)

	lp, ok := pkt.content.(*LbsPhoneInfo)
	assert.True(ok)

	assert.Equal(int16(0x01CC), lp.lbs.mcc)
	assert.Equal(byte(0x00), lp.lbs.mnc)
	assert.Equal(int16(0x266A), lp.lbs.lac)
	assert.Equal(int32(0x001DF1), lp.lbs.cell)

	expt := []byte{0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x05}
	assert.Equal(expt, lp.phone)

	rsps := buffer.NewValueBuf(64)
	lp.cmd = *NewDefCmdInfoWithValues(int32(0x10101010), "test-cmd")
	_, err = pkt.EncodeRsps(rsps)
	assert.Nil(err)

	raw2 := [...]byte{0x78, 0x78, 0x12, 0x17,
		0x0C, 0x10, 0x10, 0x10, 0x10, 0x74, 0x65, 0x73, 0x74, 0x2d, 0x63, 0x6d, 0x64,
		0x00, 0x1F, 0x72, 0x59, 0x0D, 0x0A}

	assert.Equal(raw2[:], rsps.Bytes())
}

func (ts *PackageTestSuite) TestLbsStatusInfo() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	raw := [...]byte{0x78, 0x78, 0x13, 0x19,
		0x01, 0xCC, 0x00, 0x26, 0x6A, 0x00, 0x1D, 0xF1,
		0x40, 0x06, 0x04, 0x00, 0x01, 0x01,
		0x00, 0x1F, 0xC4, 0x39, 0x0D, 0x0A}

	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	log.Infof("raw len: %d", len(raw))
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x19), pkt.ctype)
	assert.Equal(int16(0x1F), pkt.seq)

	ls, ok := pkt.content.(*LbsStatusInfo)
	assert.True(ok)

	assert.Equal(int16(0x01CC), ls.lbs.mcc)
	assert.Equal(byte(0x00), ls.lbs.mnc)
	assert.Equal(int16(0x266A), ls.lbs.lac)
	assert.Equal(int32(0x001DF1), ls.lbs.cell)

	assert.False(ls.status.buzz)
	assert.False(ls.status.fence)
	assert.False(ls.status.charge)
	assert.Equal(GT_WARN_NONE, ls.status.warn)
	assert.True(ls.status.hasPos)
	assert.Equal(byte(0x06), ls.status.vol)
	assert.Equal(GT_GSM_VERY_GOOD, ls.status.gsm)

	rsps := buffer.NewValueBuf(64)
	_, err = pkt.EncodeRsps(rsps)
	assert.Nil(err)

	raw2 := [...]byte{0x78, 0x78, 0x05, 0x19,
		0x00, 0x1F, 0x63, 0x74, 0x0D, 0x0A}

	assert.Equal(raw2[:], rsps.Bytes())
}

func (ts *PackageTestSuite) TestGpsPhoneInfo() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	raw := [...]byte{0x78, 0x78, 0x2C, 0x1A, 0x0B, 0x08, 0x1D, 0x11, 0x2E, 0x10,
		0x9C, 0x02, 0x7A, 0xC7, 0xEB, 0x0C, 0x46, 0x58, 0x49, 0x00, 0x14, 0x8F,
		0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x05,
		0x00, 0x1F, 0x84, 0xEF, 0x0D, 0x0A}

	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	log.Infof("raw len: %d", len(raw))
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x1A), pkt.ctype)
	assert.Equal(int16(0x1F), pkt.seq)

	gp, ok := pkt.content.(*GpsPhoneInfo)
	assert.True(ok)
	assert.Equal(time.Date(2000+int(0x0B), time.Month(0x08), int(0x1D), int(0x11), int(0x2E), int(0x10), 0, time.UTC), gp.date)

	assert.Equal(byte(9), gp.gps.size)
	assert.Equal(byte(12), gp.gps.num)
	assert.True(23.1117-gp.gps.lat < 0.001)
	assert.True(114.4093-gp.gps.lng < 0.001)
	assert.Equal(byte(0), gp.gps.vel)
	assert.Equal(false, gp.gps.diffPos)
	assert.Equal(true, gp.gps.hasPos)
	assert.Equal(true, gp.gps.latNorth)
	assert.Equal(false, gp.gps.lngWest)

	expt := []byte{0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x05}
	assert.Equal(expt, gp.phone)

	rsps := buffer.NewValueBuf(64)
	gp.cmd = *NewDefCmdInfoWithValues(int32(0x10101010), "test-cmd")
	_, err = pkt.EncodeRsps(rsps)
	assert.Nil(err)

	raw2 := [...]byte{0x78, 0x78, 0x12, 0x1A,
		0x0C, 0x10, 0x10, 0x10, 0x10, 0x74, 0x65, 0x73, 0x74, 0x2d, 0x63, 0x6d, 0x64,
		0x00, 0x1F, 0x84, 0xEF, 0x0D, 0x0A}

	assert.Equal(raw2[:], rsps.Bytes())
}

func (ts *PackageTestSuite) TestSettingInfo() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	raw := [...]byte{0x78, 0x78, 0x10, 0x80,
		0x0A, 0x10, 0x10, 0x10, 0x10, 0x47, 0x50, 0x53, 0x4F, 0x4E, 0x23,
		0x00, 0x1F, 0xA7, 0xF0, 0x0D, 0x0A}

	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	log.Infof("raw len: %d", len(raw))
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x80), pkt.ctype)
	assert.Equal(int16(0x1F), pkt.seq)

	si, ok := pkt.content.(*SettingInfo)
	assert.True(ok)

	assert.Equal(int32(0x10101010), si.serverId)
	assert.Equal("GPSON#", si.cmd)

	si.SetGpsOn()
	buf := buffer.NewValueBuf(64)
	_, err = pkt.EncodeRsps(buf)
	assert.Nil(err)
	assert.Equal(raw[:], buf.Bytes())

	raw2 := []byte{0x78, 0x78, 0x28, 0x80,
		0x22, 0x10, 0x10, 0x10, 0x10, 0x53, 0x4F, 0x53, 0x2C, 0x41, 0x2C, 0x31, 0x33, 0x37, 0x39, 0x30, 0x37, 0x37, 0x34, 0x30, 0x35, 0x31, 0x2C, 0x31, 0x33, 0x37, 0x39, 0x30, 0x37, 0x37, 0x34, 0x30, 0x35, 0x31, 0x23,
		0x00, 0x1F, 0x39, 0x19, 0x0D, 0x0A}
	si.SetPhones([]string{"13790774051", "13790774051"})
	buf = buffer.NewValueBuf(64)
	_, err = pkt.EncodeRsps(buf)
	assert.Nil(err)
	assert.Equal(raw2, buf.Bytes())
}

func (ts *PackageTestSuite) TestQueryInfo() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	raw := [...]byte{0x78, 0x78, 0x10, 0x81,
		0x0A, 0x10, 0x10, 0x10, 0x10, 0x47, 0x50, 0x53, 0x4F, 0x4E, 0x23,
		0x00, 0x1F, 0xD9, 0x1A, 0x0D, 0x0A}

	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	log.Infof("raw len: %d", len(raw))
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x81), pkt.ctype)
	assert.Equal(int16(0x1F), pkt.seq)

	si, ok := pkt.content.(*QueryInfo)
	assert.True(ok)

	assert.Equal(int32(0x10101010), si.serverId)
	assert.Equal("GPSON#", si.cmd)

	si.SetGpsOn()
	buf := buffer.NewValueBuf(64)
	_, err = pkt.EncodeRsps(buf)
	assert.Nil(err)
	assert.Equal(raw[:], buf.Bytes())

	raw2 := []byte{0x78, 0x78, 0x11, 0x81,
		0x0B, 0x10, 0x10, 0x10, 0x10, 0x53, 0x45, 0x45, 0x53, 0x4F, 0x53, 0x23,
		0x00, 0x1F, 0x6E, 0xD9, 0x0D, 0x0A}
	si.SetQueryPhones()
	buf = buffer.NewValueBuf(64)
	_, err = pkt.EncodeRsps(buf)
	assert.Nil(err)
	assert.Equal(raw2, buf.Bytes())
}

func (ts *PackageTestSuite) TestAgpsInfo() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())

	raw := [...]byte{0x78, 0x78, 0x0D, 0x1B,
		0x01, 0xCC, 0x00, 0x26, 0x6A, 0x00, 0x1D, 0xF1,
		0x00, 0x1F, 0x4D, 0x89, 0x0D, 0x0A}

	pkt := Gt03Pkt{}
	data := buffer.NewValueBufBytes(raw[:])
	log.Infof("raw len: %d", len(raw))
	size, err := pkt.Decode(data)
	assert.Nil(err)
	assert.Equal(size, len(raw))
	assert.Equal(int16(len(raw)-5), pkt.size)
	assert.Equal(byte(0x1B), pkt.ctype)
	assert.Equal(int16(0x1F), pkt.seq)

	ai, ok := pkt.content.(*AgpsInfo)
	assert.True(ok)

	assert.Equal(int16(0x01CC), ai.lbs.mcc)
	assert.Equal(byte(0x00), ai.lbs.mnc)
	assert.Equal(int16(0x266A), ai.lbs.lac)
	assert.Equal(int32(0x001DF1), ai.lbs.cell)

	rsps := buffer.NewValueBuf(64)
	ai.info = "SEESOS#"
	_, err = pkt.EncodeRsps(rsps)
	assert.Nil(err)

	raw2 := [...]byte{0x78, 0x78, 0x1B,
		0x00, 0x07, 0x53, 0x45, 0x45, 0x53, 0x4F, 0x53, 0x23,
		0x4D, 0x89, 0x0D, 0x0A}

	assert.Equal(raw2[:], rsps.Bytes())
}

func TestPackage(t *testing.T) {
	suite.Run(t, new(PackageTestSuite))
}
