package lb

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/keysaim/goinfra/log"
)

func init() {
	log.EnableLogRunTime(true)
	log.SetDefLevelStr("debug")
}

func TestVipElect(t *testing.T) {

	require := require.New(t)
	pctx := context.Background()
	ve := NewVipElect(pctx, "172.16.238.187", 1)
	err := ve.Start()
	require.Nil(err)
	time.Sleep(time.Second * 3)
	require.True(ve.IsLeader())
	ve.Stop()

	ve = NewVipElect(pctx, "127.0.0.1", 1)
	err = ve.Start()
	require.Nil(err)
	time.Sleep(time.Second * 3)
	require.False(ve.IsLeader())
	ve.Stop()

	ve = NewVipElect(pctx, "192.168.1.1", 1)
	err = ve.Start()
	require.Nil(err)
	time.Sleep(time.Second * 3)
	require.False(ve.IsLeader())
	ve.Stop()
}
