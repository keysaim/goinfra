package buffer

import (
	"bytes"
	"errors"
	"io"
	"unicode/utf8"
)

var BUF_FULL error
var DATA_INCOMPLETE error

func init() {
	BUF_FULL = errors.New("Buffer is full")
	DATA_INCOMPLETE = errors.New("Data in buffer is incomplete")
}

type Bufferx struct {
	buf        *bytes.Buffer
	head       int
	maxSize    int
	marginRate float32
}

func NewBufferx(buf []byte) *Bufferx {
	return &Bufferx{
		buf:        bytes.NewBuffer(buf),
		head:       0,
		maxSize:    -1,
		marginRate: 1,
	}
}

func NewBufferxString(s string) *Bufferx {
	return &Bufferx{
		buf:        bytes.NewBufferString(s),
		head:       0,
		maxSize:    -1,
		marginRate: 1,
	}
}

func (b *Bufferx) Bytes() []byte {
	return b.buf.Bytes()
}

func (b *Bufferx) Cap() int {
	return b.buf.Cap()
}

func (b *Bufferx) Grow(n int) {
	b.buf.Grow(n)
}

func (b *Bufferx) Len() int {
	return b.buf.Len()
}

func (b *Bufferx) Limit(maxSize int) (int, error) {
	lastMaxSize := b.LimitDisable()
	size, err := b.Shrink(maxSize, false)
	if err != nil {
		b.maxSize = lastMaxSize
		return 0, err
	}

	b.maxSize = size

	return size, nil
}

func (b *Bufferx) LimitDisable() (size int) {
	size = b.maxSize
	b.maxSize = -1
	return
}

func (b *Bufferx) limitCheck(toWrite int) int {
	n := b.maxSize - b.head - b.buf.Len() - toWrite
	if n < 0 {
		// it will be full, try shrink to free some space
		if b.head > 0 {
			b.Shrink(b.maxSize, false)
		}
	}
	return b.maxSize - b.head - b.buf.Len()
}

func (b *Bufferx) Next(n int) []byte {
	d := b.buf.Next(n)
	if d != nil {
		b.head += len(d)
	}
	return d
}

func (b *Bufferx) Peek(start int, size int) ([]byte, error) {
	if start >= b.Len() {
		return nil, io.EOF
	}

	end := start + size
	if end > b.Len() {
		end = b.Len()
	}
	return b.Bytes()[start:end], nil
}

func (b *Bufferx) Read(p []byte) (int, error) {
	n, err := b.buf.Read(p)
	if n > 0 {
		b.head += n
	}
	return n, err
}

func (b *Bufferx) ReadAt(p []byte, off int64) (int, error) {
	if off > int64(b.buf.Len()) {
		return 0, io.EOF
	} else {
		d := b.buf.Bytes()[off:]
		n := copy(p, d)
		if n < len(p) {
			return n, io.ErrShortBuffer
		} else {
			return n, nil
		}
	}
}

func (b *Bufferx) ReadByte() (byte, error) {
	c, err := b.buf.ReadByte()
	if err == nil {
		b.head += 1
	}
	return c, err
}

func (b *Bufferx) ReadBytes(delim byte) ([]byte, error) {
	d, err := b.buf.ReadBytes(delim)
	if d != nil {
		b.head += len(d)
	}
	return d, err
}

func (b *Bufferx) ReadFrom(r io.Reader) (int64, error) {
	n, err := b.buf.ReadFrom(r)
	return n, err
}

func (b *Bufferx) ReadRune() (rune, int, error) {
	r, n, err := b.buf.ReadRune()
	if n > 0 {
		b.head += n
	}
	return r, n, err
}

func (b *Bufferx) ReadString(delim byte) (string, error) {
	s, err := b.buf.ReadString(delim)
	b.head += len(s)
	return s, err
}

func (b *Bufferx) Reset() {
	b.buf.Reset()
	b.head = 0
}

// Try to recreate the buffer to Shrink the storage size
// If size <= 0, will keep the storage size as the data size;
// If size > 0, will make sure the shrinked storage size is the bigger one between data size and size
func (b *Bufferx) Shrink(size int, truncated bool) (int, error) {
	if b.maxSize > 0 && size > b.maxSize {
		size = b.maxSize
	}

	if size < 0 || (!truncated && size <= b.buf.Len()) {
		// If provided size < data size, keep the current storage size as the data size
		size = b.buf.Len()
	}

	buf := bytes.NewBuffer(make([]byte, 0, size))
	b.buf.WriteTo(buf)
	b.buf = buf
	b.head = 0
	return size, nil
}

func (b *Bufferx) ShrinkKeep() (int, error) {
	return b.Shrink(b.Cap(), false)
}

func (b *Bufferx) String() string {
	return b.buf.String()
}

func (b *Bufferx) Truncate(n int) {
	b.buf.Truncate(n)
}

func (b *Bufferx) writeLimit(writeFunc func(int) (int, error), toWrite int) (int, error) {
	left := b.limitCheck(toWrite)
	if left <= 0 {
		return 0, BUF_FULL
	}
	var errFull error
	if left < toWrite {
		toWrite = left
		errFull = BUF_FULL
	}
	n, err := writeFunc(toWrite)
	if errFull != nil {
		err = errFull
	}
	return n, err
}

func (b *Bufferx) Write(p []byte) (int, error) {
	if b.maxSize > 0 {
		return b.writeLimit(func(toWrite int) (int, error) {
			return b.buf.Write(p[:toWrite])
		}, len(p))
	}
	n, err := b.buf.Write(p)
	return n, err
}

func (b *Bufferx) WriteAt(p []byte, off int64) (int, error) {
	if off > int64(b.buf.Len()) {
		return 0, io.EOF
	} else {
		d := b.buf.Bytes()[off:]
		n := copy(d, p)
		if n < len(p) {
			return n, io.ErrShortWrite
		} else {
			return n, nil
		}
	}
}

func (b *Bufferx) WriteByte(c byte) error {
	if b.maxSize > 0 {
		left := b.limitCheck(1)
		if left <= 0 {
			return BUF_FULL
		}
		return b.buf.WriteByte(c)
	}
	err := b.buf.WriteByte(c)
	return err
}

func (b *Bufferx) WriteRune(r rune) (int, error) {
	if b.maxSize > 0 {
		size := utf8.RuneLen(r)
		return b.writeLimit(func(toWrite int) (int, error) {
			if toWrite < size {
				return 0, BUF_FULL
			}
			return b.buf.WriteRune(r)
		}, size)
	}
	n, err := b.buf.WriteRune(r)
	return n, err
}

func (b *Bufferx) WriteString(s string) (int, error) {
	if b.maxSize > 0 {
		return b.writeLimit(func(toWrite int) (int, error) {
			return b.buf.WriteString(s[:toWrite])
		}, len(s))
	}
	n, err := b.buf.WriteString(s)
	return n, err
}

func (b *Bufferx) WriteTo(w io.Writer) (int64, error) {
	return b.buf.WriteTo(w)
}
