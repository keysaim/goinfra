package buffer

import (
	"encoding/binary"
	"testing"

	_ "github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"

	"gitlab.com/keysaim/goinfra/log"
)

type ValueBufTestSuite struct {
	suite.Suite
}

func (ts *ValueBufTestSuite) SetupSuite() {
	log.EnableLogRunTime(true)
	log.Info("SetupSuite")
}

func (ts *ValueBufTestSuite) TearDownSuite() {
	log.Info("TeardownSuite")
}

func (ts *ValueBufTestSuite) TestWriteReadInt16() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())
	data := [...]int16{1000, 2000, 3000, 4000, 5000, 6000}
	buf := NewValueBuf(64)

	for _, val := range data {
		err := buf.WriteInt16(binary.LittleEndian, val)
		assert.Nil(err)
	}
	assert.Equal(len(data)*2, buf.Len())

	for _, val := range data {
		rv, err := buf.ReadInt16(binary.LittleEndian)
		assert.Nil(err)
		assert.Equal(val, rv)
	}
	assert.Equal(0, buf.Len())
}

func (ts *ValueBufTestSuite) TestWriteReadUint16() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())
	data := [...]uint16{1000, 2000, 3000, 4000, 5000, 6000}
	buf := NewValueBuf(64)

	for _, val := range data {
		err := buf.WriteUint16(binary.LittleEndian, val)
		assert.Nil(err)
	}
	assert.Equal(len(data)*2, buf.Len())

	for _, val := range data {
		rv, err := buf.ReadUint16(binary.LittleEndian)
		assert.Nil(err)
		assert.Equal(val, rv)
	}
	assert.Equal(0, buf.Len())
}

func (ts *ValueBufTestSuite) TestWriteReadInt32() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())
	data := [...]int32{1000, 2000, 3000, 4000, 5000, 6000}
	buf := NewValueBuf(64)

	for _, val := range data {
		err := buf.WriteInt32(binary.LittleEndian, val)
		assert.Nil(err)
	}
	assert.Equal(len(data)*4, buf.Len())

	for _, val := range data {
		rv, err := buf.ReadInt32(binary.LittleEndian)
		assert.Nil(err)
		assert.Equal(val, rv)
	}
	assert.Equal(0, buf.Len())
}

func (ts *ValueBufTestSuite) TestWriteReadUint32() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())
	data := [...]uint32{1000, 2000, 3000, 4000, 5000, 6000}
	buf := NewValueBuf(64)

	for _, val := range data {
		err := buf.WriteUint32(binary.LittleEndian, val)
		assert.Nil(err)
	}
	assert.Equal(len(data)*4, buf.Len())

	for _, val := range data {
		rv, err := buf.ReadUint32(binary.LittleEndian)
		assert.Nil(err)
		assert.Equal(val, rv)
	}
	assert.Equal(0, buf.Len())
}

func (ts *ValueBufTestSuite) TestWriteReadInt64() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())
	data := [...]int64{1000, 2000, 3000, 4000, 5000, 6000}
	buf := NewValueBuf(64)

	for _, val := range data {
		err := buf.WriteInt64(binary.LittleEndian, val)
		assert.Nil(err)
	}
	assert.Equal(len(data)*8, buf.Len())

	for _, val := range data {
		rv, err := buf.ReadInt64(binary.LittleEndian)
		assert.Nil(err)
		assert.Equal(val, rv)
	}
	assert.Equal(0, buf.Len())
}

func (ts *ValueBufTestSuite) TestWriteReadUint64() {
	//assert := assert.New(ts.T())
	assert := require.New(ts.T())
	data := [...]uint64{1000, 2000, 3000, 4000, 5000, 6000}
	buf := NewValueBuf(64)

	for _, val := range data {
		err := buf.WriteUint64(binary.LittleEndian, val)
		assert.Nil(err)
	}
	assert.Equal(len(data)*8, buf.Len())

	for _, val := range data {
		rv, err := buf.ReadUint64(binary.LittleEndian)
		assert.Nil(err)
		assert.Equal(val, rv)
	}
	assert.Equal(0, buf.Len())
}

func TestValueBuf(t *testing.T) {
	suite.Run(t, new(ValueBufTestSuite))
}
