package dlock

import (
	"os"
	"strings"

	capi "github.com/hashicorp/consul/api"
	"gitlab.com/keysaim/goinfra/consul"
	"gitlab.com/keysaim/goinfra/log"
	"gitlab.com/keysaim/goinfra/utils"
)

var hostname string

func init() {
	h, err := os.Hostname()
	if err != nil {
		log.Errorf("Failed to get the hostname, error: %v", err)
	}
	hostname = h
}

type ConsulCasDlock struct {
	kv      *consul.ConsulKV
	key     string
	timeout int
	owner   string
	freeze  bool
}

func NewConsulCasDlock(c *capi.Client, key string, timeout int, owner string) *ConsulCasDlock {

	if owner == "" {
		owner = hostname + "|" + utils.RandString(5)
	}
	return &ConsulCasDlock{
		kv:      consul.NewConsulKV(c.KV()),
		key:     key,
		timeout: timeout,
		owner:   owner,
	}
}

func (l *ConsulCasDlock) Key() string {
	return l.key
}

func (l *ConsulCasDlock) SetOwner(owner string) {
	l.owner = owner
}

func (l *ConsulCasDlock) Timeout() int {
	return l.timeout
}

func (l *ConsulCasDlock) SetFreeze() {
	l.freeze = true
}

func (l *ConsulCasDlock) Lock() (o string, err error) {

	if l.freeze {
		o, err = l.kv.FreezeLock(l.key, l.owner, l.timeout)
	} else {
		o, err = l.kv.Lock(l.key, l.owner, l.timeout)
	}
	if err == consul.ErrLockIsFrozen {
		return o, ErrLockIsFrozen
	}
	if err == consul.ErrLockIsHold {
		return o, ErrLockIsHold
	}
	return o, err
}

func (l *ConsulCasDlock) Relock() (string, error) {
	return l.Lock()
}

func (l *ConsulCasDlock) Unlock() error {
	_, err := l.kv.Unlock(l.key, l.owner)
	if err == consul.ErrLockIsHold {
		return ErrLockIsHold
	}
	return err
}

func (l *ConsulCasDlock) Destroy() error {
	return l.kv.Del(l.key)
}

func (l *ConsulCasDlock) Owner() (string, error) {

	return l.getOwner()
}

func (l *ConsulCasDlock) getOwner() (string, error) {

	kvp, _, err := l.kv.KV().Get(l.key, nil)
	if err != nil {
		log.Errorf("Failed to get the value from key %s, error: %v", l.key, err)
		return "", err
	}
	if kvp == nil {
		return "", nil
	}

	return strings.Split(string(kvp.Value), ":")[0], nil
}
