package tcp

import (
	"fmt"
	"io"
	"net"
	"strings"
	"sync"
	"time"

	"gitlab.com/keysaim/goinfra/log"
)

type TcpServer struct {
	address  string
	port     int
	exitCg   *sync.WaitGroup
	listener net.Listener
}

func NewTcpServerWithAddress(address string, port int) *TcpServer {
	return &TcpServer{
		address: address,
		port:    port,
		exitCg:  &sync.WaitGroup{},
	}
}

func NewTcpServer(port int) *TcpServer {
	return NewTcpServerWithAddress("", port)
}

func (ts *TcpServer) Start(connChan chan *TcpConn) error {
	addr := ""
	if ts.address != "" {
		addr = fmt.Sprintf("%s:%d", ts.address, ts.port)
	} else {
		addr = fmt.Sprintf(":%d", ts.port)
	}

	l, err := net.Listen("tcp", addr)
	if err != nil {
		log.Error("Tcp server failed to listen: ", err)
		return err
	}
	log.Infof("Tcp server is listening on address: %s", addr)
	ts.listener = l
	ts.exitCg.Add(1)

	go func() {
		defer func() {
			l.Close()
			ts.exitCg.Done()
		}()

		log.Infof("Tcp server accept loop started")
		for {
			conn, err := l.Accept()
			if err != nil {
				if conn != nil {
					log.Error("BUG: net.Listener returned non-nil conn and non-nil error")
					return
				}
				if netErr, ok := err.(net.Error); ok && netErr.Temporary() {
					log.Infof("Temporary error when accepting new connections: %s", netErr)
					time.Sleep(time.Second)
					continue
				}
				if err != io.EOF && !strings.Contains(err.Error(), "use of closed network connection") {
					log.Errorf("Permanent error when accepting new connections: %s", err)
					return
				}

				log.Infof("Tcp Server is closed, reason: %s", err)
				return
			}

			log.Infof("Tcp server received one connection %v", conn)
			tc := NewTcpConn(conn)
			connChan <- tc
		}
	}()

	return nil
}

func (ts *TcpServer) Stop() {
	if ts.listener != nil {
		ts.listener.Close()
		ts.listener = nil
	}
	ts.exitCg.Wait()
	log.Info("Tcp Server stopped")
}
