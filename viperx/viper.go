package viperx

import (
	"github.com/spf13/viper"
)

func GetString(name string, def string) string {

	if !viper.IsSet(name) {
		return def
	}
	return viper.GetString(name)
}

func GetInt(name string, def int) int {

	if !viper.IsSet(name) {
		return def
	}
	return viper.GetInt(name)
}

func GetIntSlice(name string, def []int) []int {

	if !viper.IsSet(name) {
		return def
	}
	v := viper.Get(name)
	vl, ok := v.([]interface{})
	if !ok {
		return def
	}
	il := make([]int, len(vl))
	for i, v := range vl {
		if a, ok := v.(int); !ok {
			return def
		} else {
			il[i] = a
		}
	}
	return il
}

func GetBool(name string, def bool) bool {

	if !viper.IsSet(name) {
		return def
	}
	return viper.GetBool(name)
}
