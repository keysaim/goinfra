package consul

import (
	"context"
	"testing"
	"time"

	capi "github.com/hashicorp/consul/api"
	"github.com/stretchr/testify/require"
	"gitlab.com/keysaim/goinfra/log"
)

var client *capi.Client

func init() {
	log.EnableLogRunTime(true)
	log.SetDefLevelStr("debug")

	c, err := capi.NewClient(&capi.Config{
		Address: "127.0.0.1:8500",
	})
	if err != nil {
		log.Fatal(err)
	}
	client = c
}

func TestElect(t *testing.T) {

	require := require.New(t)
	pctx := context.Background()
	key := "test/leader/"
	kv := NewConsulKV(client.KV())
	err := kv.Put(key, []byte(""))
	require.Nil(err)
	var el []*Elector
	for i := 0; i < 1; i += 1 {
		//e := NewElector(pctx, client, fmt.Sprintf("test%s", i), key, 1)
		e := NewElector(pctx, client, "", key, 1)
		err := e.Start()
		require.Nil(err)
		el = append(el, e)
	}

	time.Sleep(time.Second * 10)
	cnt := 0
	for _, e := range el {
		if e.IsLeader() {
			cnt += 1
		}
	}
	require.Equal(cnt, 1)
}
