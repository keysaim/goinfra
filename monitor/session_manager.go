package monitor

import (
	"sync"

	"github.com/jmoiron/sqlx"

	"gitlab.com/keysaim/goinfra/log"
	"gitlab.com/keysaim/goinfra/netx/tcp"
)

type SessionManager struct {
	db          *sqlx.DB
	server      *tcp.TcpServer
	connChan    chan *tcp.TcpConn
	exitWg      *sync.WaitGroup
	sessionWg   *sync.WaitGroup
	sessionMap  map[*Session]*Context
	ctxExitChan chan *Context
	exitChan    chan struct{}
	sampleUrl   string
}

func NewSessionManager(db *sqlx.DB, server *tcp.TcpServer, sampleUrl string) *SessionManager {
	return &SessionManager{
		db:          db,
		server:      server,
		connChan:    make(chan *tcp.TcpConn),
		exitWg:      &sync.WaitGroup{},
		sessionWg:   &sync.WaitGroup{},
		sessionMap:  make(map[*Session]*Context),
		ctxExitChan: make(chan *Context),
		exitChan:    make(chan struct{}),
		sampleUrl:   sampleUrl,
	}
}

func (sm *SessionManager) SessionCount() int {
	return len(sm.sessionMap)
}

func (sm *SessionManager) Start() error {
	err := sm.server.Start(sm.connChan)
	if err != nil {
		log.Errorf("Failed to start session manager, error: %s", err)
		return err
	}

	// Start the connection loop
	sm.exitWg.Add(1)
	go func() {
		log.Info("Connection loop started")
		defer func() {
			sm.exitWg.Done()
			log.Info("Connection loop stopped")
		}()

		for {
			select {
			case <-sm.exitChan:
				return
			case conn := <-sm.connChan:
				if conn != nil {
					sm.onConnReceived(conn)
				} else {
					log.Info("Connection channel closed, connection loop will return")
				}
			case ctx := <-sm.ctxExitChan:
				if ctx != nil {
					sm.onCtxExited(ctx)
				}
			}
		}
	}()

	return nil
}

func (sm *SessionManager) Stop() {
	sm.server.Stop()

	// Release all sessions
	for _, ctx := range sm.sessionMap {
		ctx.Stop()
	}

	sm.sessionWg.Wait()
	sm.exitChan <- struct{}{}
	sm.exitWg.Wait()
	close(sm.connChan)
}

func (sm *SessionManager) onConnReceived(conn *tcp.TcpConn) {
	sn := NewSession(conn)
	ctx := NewContext(sm.db, sn, sm.sampleUrl)

	log.Infof("Session %p created with context %p", sn, ctx)
	sm.sessionWg.Add(1)
	sm.sessionMap[sn] = ctx
	ctx.Start(sm.ctxExitChan)
}

func (sm *SessionManager) onCtxExited(ctx *Context) {
	sn := ctx.Session()
	log.Infof("Context %p exited, remove session %p", ctx, sn)
	if _, ok := sm.sessionMap[sn]; ok {
		delete(sm.sessionMap, sn)
	}
	sm.sessionWg.Done()
}
