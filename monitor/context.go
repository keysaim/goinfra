package monitor

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"

	_ "database/sql"
	"github.com/jmoiron/sqlx"

	"../httpx"
	"gitlab.com/keysaim/goinfra/log"
	"../protocol/gt03"
)

type Context struct {
	db         *sqlx.DB
	sn         *Session
	client     *httpx.HttpClient
	sampleUrl  string
	snExitChan chan error
	msgChan    chan *gt03.Gt03Pkt
	exitErr    error

	imei    string
	carId   int
	usageId int
	power   int
}

func NewContext(db *sqlx.DB, sn *Session, sampleUrl string) *Context {
	return &Context{
		db:         db,
		sn:         sn,
		client:     httpx.NewHttpClient(nil),
		sampleUrl:  sampleUrl,
		snExitChan: make(chan error),
		msgChan:    make(chan *gt03.Gt03Pkt),
		power:      -1,
	}
}

func (c *Context) Session() *Session {
	return c.sn
}

func (c *Context) ExitErr() error {
	return c.exitErr
}

func (c *Context) Start(exitChan chan<- *Context) {
	c.sn.Start(c.msgChan, c.snExitChan)
	go c.run(exitChan)
}

func (c *Context) Stop() {
	c.sn.Stop()
}

func (c *Context) run(exitChan chan<- *Context) {
	for {
		select {
		case msg := <-c.msgChan:
			c.onMsg(msg)
		case err := <-c.snExitChan:
			log.Infof("Session exited with err: %v, exit the content in client: %s", err, c.imei)
			c.exitErr = err
			exitChan <- c
			return
		}
	}
}

func (c *Context) onMsg(pkt *gt03.Gt03Pkt) {
	var err error

	switch pkt.Type() {
	case gt03.GT_INFO_LOGIN:
		err = c.onLogin(pkt)
	case gt03.GT_INFO_GPS:
		err = c.onGps(pkt)
	case gt03.GT_INFO_LBS:
		err = c.onLbs(pkt)
	case gt03.GT_INFO_STATUS:
		err = c.onStatus(pkt)
	case gt03.GT_INFO_GPS_LBS_STATUS:
		err = c.onGpsLbsStatus(pkt)
	case gt03.GT_INFO_LBS_PHONE:
		err = c.onLbsPhone(pkt)
	case gt03.GT_INFO_EXT_LBS:
		err = c.onExtLbs(pkt)
	case gt03.GT_INFO_LBS_STATUS:
		err = c.onLbsStatus(pkt)
	case gt03.GT_INFO_GPS_PHONE:
		err = c.onGpsPhone(pkt)
	case gt03.GT_INFO_SETTING:
		err = c.onSetting(pkt)
	case gt03.GT_INFO_QUERY:
		err = c.onQuery(pkt)
	case gt03.GT_INFO_AGPS:
		err = c.onAgps(pkt)
	default:
		log.Errorf("Invalid msg type: %d in client: %s", pkt.Type(), c.imei)
		err = errors.New("Invalid msg type")
	}

	if err != nil {
		c.sn.ReplyError(pkt)
	}
}

func (c *Context) onLogin(pkt *gt03.Gt03Pkt) error {
	lg := pkt.Content().(*gt03.LoginInfo)
	log.Infof("Received login info: %#v in client: %s", lg, c.imei)
	c.imei = lg.Imei()

	err := c.queryCarInfo()
	if err != nil {
		return err
	}

	err = c.sn.Reply(pkt)
	if err != nil {
		log.Errorf("Failed to reply login request in client: %s", c.imei)
		return err
	}
	return nil
}

func (c *Context) queryCarInfo() error {
	var carId int = 0
	row := c.db.QueryRowx("SELECT car_id FROM client_info where serial=?", c.imei)
	if row == nil {
		log.Errorf("Failed to find the client with imei: %s", c.imei)
		//TODO need error handle
		return errors.New("Not found client")
	}
	err := row.Scan(&carId)
	if err != nil {
		log.Error("Query car_id failed with error:", err, " in client:", c.imei)
		//TODO need error handle
		return err
	}
	c.carId = carId
	return nil
}

func (c *Context) onGps(pkt *gt03.Gt03Pkt) error {
	gps := pkt.Content().(*gt03.GpsInfo)
	log.Infof("Received gps info: %#v in client: %s", gps, c.imei)
	//lat := gps.Lat()
	//lng := gps.Lng()

	//_, err := c.db.Exec("UPDATE real_time_info SET latitude=?, longitude=? where car_id=?", lat, lng, c.carId)
	//if err != nil {
	//TODO need error handle
	//		return
	//}
	rinfo := c.getRtInfo(gps)
	if rinfo == nil {
		err := fmt.Sprintf("The client: %s is not assigned to a car", c.imei)
		log.Warn(err)
		return errors.New(err)
	}
	stats, err := c.client.PostJson(c.sampleUrl, rinfo)
	if err != nil {
		log.Errorf("Failed to post sample data: %#v to url: %s, err: %s in client: %s", rinfo, c.sampleUrl, err, c.imei)
		return err
	}

	body, err := ioutil.ReadAll(stats.Resp.Body)
	if err != nil {
		log.Errorf("Failed to read sample resp, data: %#v to url: %s, err: %s in client: %s", rinfo, c.sampleUrl, err, c.imei)
		return err
	}

	resp := RtResp{}
	err = json.Unmarshal(body, &resp)
	if err != nil {
		log.Errorf("Failed to parse json response: %s in client: %s", body, c.imei)
		return err
	}

	if resp.Code == "200" {
		c.carId = resp.GetCarId()
		c.usageId = resp.GetUsageId()
		log.Infof("Context: %#v", c)
	}

	err = c.sn.Reply(pkt)
	if err != nil {
		log.Errorf("Failed to reply gps request in client: %s", c.imei)
		return err
	}
	return nil
}

func (c *Context) getRtInfo(gps *gt03.GpsInfo) *RtInfo {
	if c.carId < 0 {
		err := c.queryCarInfo()
		if err != nil {
			return nil
		}
	}

	rinfo := &RtInfo{}

	rinfo.Series = c.imei
	rinfo.CarId = c.carId
	rinfo.UsageId = c.usageId
	rinfo.Power = c.power
	rinfo.Lat = gps.Lat()
	rinfo.Lng = gps.Lng()
	rinfo.Speed = int(gps.Vel())
	rinfo.Realtime = gps.Date().Format("2006-01-02 15:04:05")
	rinfo.Client = "gt03"
	return rinfo
}

func (c *Context) onLbs(pkt *gt03.Gt03Pkt) error {
	return errors.New("Not implement")
}

func (c *Context) onStatus(pkt *gt03.Gt03Pkt) error {
	status := pkt.Content().(*gt03.StatusInfo)
	log.Infof("Received status info: %#v in client: %s", status, c.imei)

	sinfo := &StatusInfo{}
	sinfo.Power = int(status.Vol())
	sinfo.Series = c.imei

	url := fmt.Sprintf("%s?type=status", c.sampleUrl)
	_, err := c.client.PostJson(url, sinfo)
	if err != nil {
		log.Errorf("Failed to post sample data: %#v to url: %s, err: %s in client: %s", sinfo, url, err, c.imei)
		return err
	}

	err = c.sn.Reply(pkt)
	if err != nil {
		log.Errorf("Failed to reply gps request in client: %s", c.imei)
		return err
	}
	return nil
}

func (c *Context) onGpsLbsStatus(pkt *gt03.Gt03Pkt) error {
	gls := pkt.Content().(*gt03.GpsLbsStatusInfo)
	log.Infof("Received GpsLbsStatusInfo: %#v in client: %s", gls, c.imei)
	if gls.Status() != nil {
		c.power = int(gls.Status().Vol())
	}
	return nil
}

func (c *Context) onLbsPhone(pkt *gt03.Gt03Pkt) error {
	return errors.New("Not implement")
}

func (c *Context) onExtLbs(pkt *gt03.Gt03Pkt) error {
	return errors.New("Not implement")
}

func (c *Context) onLbsStatus(pkt *gt03.Gt03Pkt) error {
	ls := pkt.Content().(*gt03.LbsStatusInfo)
	log.Infof("Received LbsStatusInfo: %#v in client: %s", ls, c.imei)
	if ls.Status() != nil {
		c.power = int(ls.Status().Vol())
	}
	return nil
}

func (c *Context) onGpsPhone(pkt *gt03.Gt03Pkt) error {
	return errors.New("Not implement")
}

func (c *Context) onSetting(pkt *gt03.Gt03Pkt) error {
	return errors.New("Not implement")
}

func (c *Context) onQuery(pkt *gt03.Gt03Pkt) error {
	return errors.New("Not implement")
}

func (c *Context) onAgps(pkt *gt03.Gt03Pkt) error {
	return errors.New("Not implement")
}
