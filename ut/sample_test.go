package ut

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github4-chn.cisco.com/OMD/content_prepos/src/log"
	"testing"
)

type SampleTestSuite struct {
	suite.Suite
}

func (ts *SampleTestSuite) SetupSuite() {
	log.Register("ut", log.InfoLevel)
	log.SetDefLevel(log.ErrorLevel)
	log.Md("ut").Info("SetupSuite")
}

func (ts *SampleTestSuite) TearDownSuite() {
	log.Md("ut").Info("TeardownSuite")
}

func (ts *SampleTestSuite) SetupTest() {
	log.Info("SetupTest")
}

func (ts *SampleTestSuite) TearDownTest() {
	log.Info("TeardownTest")
}

func (ts *SampleTestSuite) Testcase1() {
	log.Info("Testcase1")
	sp := new(Sample)
	assert := assert.New(ts.T())
	require := require.New(ts.T())

	assert.Equal("wangwang", sp.SayHello("dog"), "dog should say wangwang")
	assert.NotEqual("miaomiao", sp.SayHello("dog"), "dog should not say miaomiao")
	require.Equal("wangwang", sp.SayHello("dog"), "dog must say wangwang")
	require.NotEqual("miaomiao", sp.SayHello("dog"), "dog must not say miaomiao")
}

func (ts *SampleTestSuite) Testcase2() {
	log.Info("Testcase2")
	sp := new(Sample)
	assert := assert.New(ts.T())
	require := require.New(ts.T())

	assert.NotEqual("wangwang", sp.SayHello("cat"), "cat should not say wangwang")
	assert.Equal("miaomiao", sp.SayHello("cat"), "cat should say miaomiao")
	require.NotEqual("wangwang", sp.SayHello("cat"), "cat must not say wangwang")
	require.Equal("miaomiao", sp.SayHello("cat"), "cat must say miaomiao")
}

func TestSampleTestSuite(t *testing.T) {
	suite.Run(t, new(SampleTestSuite))
}
