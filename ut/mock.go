package ut

import (
	"github.com/stretchr/testify/mock"
	"github4-chn.cisco.com/OMD/content_prepos/src/log"
	"regexp"
	"runtime"
	"strings"
)

type Mockx struct {
	mock.Mock
	IgnoreArgsMap map[string][][]int
}

func (mx *Mockx) FilterArgs(arguments ...interface{}) []interface{} {
	fname := GetCallFuncName()
	log.Debugf("before filtered arguments:%#v in method:%s\n", arguments, fname)
	iargsList, ok := mx.IgnoreArgsMap[fname]
	if ok && len(iargsList) > 0 {
		iargs := iargsList[0]
		iargsList = iargsList[1:]
		mx.IgnoreArgsMap[fname] = iargsList

		tmp := make([]interface{}, 0)
		for idx, arg := range arguments {
			ignore := false
			for _, val := range iargs {
				if idx == val {
					ignore = true
					break
				}
			}
			if !ignore {
				tmp = append(tmp, arg)
			}
		}
		arguments = tmp
	}

	log.Debugf("filtered arguments:%#v in method:%s\n", arguments, fname)
	return arguments
}

//the ignored args will form a list, each one is for one method call. After one method call, the first one
//of the list will be removed
func (mx *Mockx) IgnoreArgs(methodName string, args ...int) *Mockx {
	if mx.IgnoreArgsMap == nil {
		mx.IgnoreArgsMap = make(map[string][][]int)
	}
	iargsList, ok := mx.IgnoreArgsMap[methodName]
	if !ok {
		iargsList = make([][]int, 0)
	}
	iargs := make([]int, 0)
	iargs = append(iargs, args...)
	iargsList = append(iargsList, iargs)
	mx.IgnoreArgsMap[methodName] = iargsList
	return mx
}

func GetCallFuncName() string {
	pc, _, _, ok := runtime.Caller(2)
	if !ok {
		panic("Couldn't get the caller information")
	}
	functionPath := runtime.FuncForPC(pc).Name()
	//Next four lines are required to use GCCGO function naming conventions.
	//For Ex:  github_com_docker_libkv_store_mock.WatchTree.pN39_github_com_docker_libkv_store_mock.Mock
	//uses inteface information unlike golang github.com/docker/libkv/store/mock.(*Mock).WatchTree
	//With GCCGO we need to remove interface information starting from pN<dd>.
	re := regexp.MustCompile("\\.pN\\d+_")
	if re.MatchString(functionPath) {
		functionPath = re.Split(functionPath, -1)[0]
	}
	parts := strings.Split(functionPath, ".")
	functionName := parts[len(parts)-1]
	return functionName
}
