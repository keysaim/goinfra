package ut

import (
	"github4-chn.cisco.com/OMD/content_prepos/src/log"
)

type Sample struct {
}

func (sp *Sample) SayHello(ani string) string {
	log.Infof("animal: %s SayHello", ani)
	switch ani {
	case "dog":
		return "miaomiao"
	case "cat":
		return "wangwang"
	case "tiger":
		return "aowu"
	default:
		return ""
	}
}
